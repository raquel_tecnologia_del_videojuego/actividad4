//Maya ASCII 2023 scene
//Name: actividad.ma
//Last modified: Sun, Jan 22, 2023 10:46:46 AM
//Codeset: UTF-8
requires maya "2023";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiStandardSurface"
		 -nodeType "aiStandardHair" -nodeType "aiShadowMatte" "mtoa" "5.2.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202208031415-1dee56799d";
fileInfo "osv" "Mac OS X 10.16";
fileInfo "UUID" "C4EF8139-EE45-995B-830A-14913F8FDE8E";
createNode transform -s -n "persp";
	rename -uid "FF3488AF-9645-2E7F-8E04-668526A8E065";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 779.15380353890419 218.33742663839394 5775.9525404729093 ;
	setAttr ".r" -type "double3" -1075.5489313240855 9367.842750984486 5.0165398932947185e-17 ;
	setAttr ".rp" -type "double3" -2.2737367544323206e-13 1.1368683772161603e-13 0 ;
	setAttr ".rpt" -type "double3" 5.4785046476548691e-14 -9.3422643700293982e-15 1.410218719862506e-13 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "F827DDC7-A64E-14D0-D153-0D932BE7116D";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 6204.1871422188069;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 325.01836116767669 6.0918445790036913e-06 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "9CF3ED76-9249-D0FB-A599-D3A05569334A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.0218035499214748 671.16521613177463 1506.7042037454803 ;
	setAttr ".rpt" -type "double3" -4.4275356867563063e-14 -4.0005036457862577e-14 1.1526472146719325e-14 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "17E2EF7A-D642-AA78-BBDF-1285B2A45575";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 1403.0362412964664;
	setAttr ".ow" 5461.2694908334206;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 2.0218035499214304 671.16521613177463 103.66796244901388 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "236067BD-D341-46F8-BFB0-1F93EE4B30CE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1195.4712191010187 409.46063702308669 -1099.360411673651 ;
	setAttr ".r" -type "double3" 0 89.999999999999972 0 ;
	setAttr ".rp" -type "double3" 0 -5.6843418860808015e-14 0 ;
	setAttr ".rpt" -type "double3" -5.6397190124629376e-17 1.3436432371732314e-15 4.2948846140876524e-15 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2BAAF717-274C-60BC-8F17-6FB4B7ECD645";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1000000000008;
	setAttr ".ow" 6465.9425248781636;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 195.37121910101791 409.46063702308669 -1099.3604116736512 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "36F09A67-0C44-EE57-2D4F-29BEE2EAC05C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 195.37121910101794 1409.5606370230885 -1099.360411673651 ;
	setAttr ".r" -type "double3" -89.999999999999972 89.999999999999972 0 ;
	setAttr ".rpt" -type "double3" -3.5661433287255378e-14 -4.0378439077890445e-14 3.1972184617766365e-14 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "11BB54B0-C547-5CA4-31C3-11AADBC70533";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1000000000017;
	setAttr ".ow" 9215.9313729507467;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 195.37121910101769 409.46063702308675 -1099.360411673651 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "House1";
	rename -uid "67F6A6ED-9142-5977-AE32-F097EBA22ED5";
	setAttr ".rp" -type "double3" -337.35681957613059 251.13749736613346 273.80646006715659 ;
	setAttr ".sp" -type "double3" -337.35681957613059 251.13749736613346 273.80646006715659 ;
createNode mesh -n "House1Shape" -p "House1";
	rename -uid "DAF2C5EE-A545-B180-99DA-0C9861A80926";
	setAttr -k off ".v";
	setAttr -s 5 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[6]";
	setAttr ".iog[0].og[3].gcl" -type "componentList" 1 "f[5]";
	setAttr ".iog[0].og[4].gcl" -type "componentList" 4 "f[0]" "f[2]" "f[4]" "f[8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5625 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -746.09943 40.235138 629.93622 
		71.385803 40.235138 629.93622 -746.09943 462.03986 629.93622 71.385803 462.03986 
		629.93622 -746.09943 462.03986 -82.323288 71.385803 462.03986 -82.323288 -746.09943 
		40.235138 -82.323288 71.385803 40.235138 -82.323288 -337.35681 40.235138 629.93622 
		-337.35681 721.81818 629.93622 -337.35681 721.81818 -82.323288 -337.35681 40.235138 
		-82.323288;
	setAttr -s 12 ".vt[0:11]"  -0.81733882 -0.66964674 1.031907082 0.81733882 -0.66964674 1.031907082
		 -0.81733882 0.66964674 1.031907082 0.81733882 0.66964674 1.031907082 -0.81733882 0.66964674 -1.031907082
		 0.81733882 0.66964674 -1.031907082 -0.81733882 -0.66964674 -1.031907082 0.81733882 -0.66964674 -1.031907082
		 0 -0.66964674 1.031907082 0 0.66964674 1.031907082 0 0.66964674 -1.031907082 0 -0.66964674 -1.031907082;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "HouseRoof";
	rename -uid "EDCCD3CD-9C4A-6BA6-74BE-20A2F5A1D44D";
	setAttr ".rp" -type "double3" -319.61929146253658 651.40742537156507 284.11750322017872 ;
	setAttr ".sp" -type "double3" -319.61929146253658 651.40742537156507 284.11750322017872 ;
createNode mesh -n "HouseRoofShape" -p "HouseRoof";
	rename -uid "C59BD109-4D4F-525A-9AB5-589CFA9FD923";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 4 "f[0]" "f[2]" "f[4:6]" "f[8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.1875 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -438.94315 532.08356 438.67038 
		-200.29544 532.08356 438.67038 -438.94315 770.73126 438.67038 -200.29544 770.73126 
		438.67038 -438.94315 770.73126 129.56462 -200.29544 770.73126 129.56462 -438.94315 
		532.08356 129.56462 -200.29544 532.08356 129.56462 -438.94315 870.4176 284.11749 
		-438.94315 532.08356 284.11749 -200.29544 532.08356 284.11749 -200.29544 870.4176 
		284.11749 0 0 -2.1175824e-22 0 0 4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Floor";
	rename -uid "F97F459F-ED47-BE31-CDCA-A2B419AA47B1";
	setAttr ".rp" -type "double3" 336.27952910362183 0 -206.61132432596347 ;
	setAttr ".sp" -type "double3" 336.27952910362183 0 -206.61132432596347 ;
createNode mesh -n "FloorShape" -p "Floor";
	rename -uid "8EE76686-554F-8E28-FBFD-AE8E691F0900";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.6 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 24 ".uvst[0].uvsp[0:23]" -type "float2" 0.37584975 0.99769288
		 0.37584975 0.50173336 0.40466368 0.50173336 0.40466368 0.99769288 0.34172329 0.99782795
		 0.34172329 0.50173336 0.3703571 0.50173336 0.3703571 0.99782795 0.037376329 0.49813163
		 0.037376329 0.0019920322 0.5333358 0.0019920322 0.5333358 0.49813163 0.5102914 0.50191343
		 1.0063859224 0.50191343 1.0063859224 0.99800801 0.5102914 0.99800801 0.0018091537
		 0.49804157 0.0018091537 0.0019920322 0.030442983 0.0019920322 0.030442983 0.49804157
		 0.54004407 0.4983117 0.54004407 0.0019920322 0.56885797 0.0019920322 0.56885797 0.4983117;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1185.8511 -54.909473 1315.5193 
		1858.4102 -54.909473 1315.5193 -1185.8511 54.909473 1315.5193 1858.4102 54.909473 
		1315.5193 -1185.8511 54.909473 -1728.7419 1858.4102 54.909473 -1728.7419 -1185.8511 
		-54.909473 -1728.7419 1858.4102 -54.909473 -1728.7419;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 16 17 18 19
		f 4 1 7 -3 -7
		mu 0 4 8 9 10 11
		f 4 2 9 -4 -9
		mu 0 4 20 21 22 23
		f 4 3 11 -1 -11
		mu 0 4 12 13 14 15
		f 4 -12 -10 -8 -6
		mu 0 4 0 1 2 3
		f 4 10 4 6 8
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Human";
	rename -uid "3C64D654-054C-9140-BBE0-FA9704161142";
	setAttr ".rp" -type "double3" 2056.0119608249056 623.80341584370069 1698.2827893582717 ;
	setAttr ".sp" -type "double3" 2056.0119608249056 623.80341584370069 1698.2827893582717 ;
createNode mesh -n "HumanShape" -p "Human";
	rename -uid "3DE68D0B-C942-C859-2E11-B6B96DE8A025";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  2036.512 536.80341 1717.7828 
		2075.512 536.80341 1717.7828 2036.512 710.80341 1717.7828 2075.512 710.80341 1717.7828 
		2036.512 710.80341 1678.7828 2075.512 710.80341 1678.7828 2036.512 536.80341 1678.7828 
		2075.512 536.80341 1678.7828;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Cover1";
	rename -uid "7B030DC3-EB43-D621-2176-2095A2D50909";
	setAttr ".rp" -type "double3" -334.18063478280152 392.53751349393127 768.87125361577318 ;
	setAttr ".sp" -type "double3" -334.18063478280152 392.53751349393127 768.87125361577318 ;
createNode mesh -n "CoverShape1" -p "Cover1";
	rename -uid "ABE605F2-2F4A-6C00-BCB3-ACA1C617E25E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -641.8667 313.34537 939.23193 
		-26.494587 313.34537 939.23193 -641.8667 343.3457 950.50671 -26.494587 343.3457 950.50671 
		-641.8667 471.72968 598.51056 -26.494587 471.72968 598.51056 -641.8667 441.72931 
		587.23584 -26.494587 441.72931 587.23584;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column2";
	rename -uid "CE0CD782-1843-555B-276D-F4B2678C0E78";
	setAttr ".rp" -type "double3" -37.053183652378863 170.27541601398588 898.60596284785959 ;
	setAttr ".sp" -type "double3" -37.053183652378863 170.27541601398588 898.60596284785959 ;
createNode mesh -n "Column2Shape" -p "Column2";
	rename -uid "FD48B4CE-7545-39CD-B07F-C3B72CFBDE68";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -56.553185 7.0098209 918.10596 
		-17.553185 7.0098209 918.10596 -56.553185 333.54102 918.10596 -17.553185 333.54102 
		918.10596 -56.553185 333.54102 879.10596 -17.553185 333.54102 879.10596 -56.553185 
		7.0098209 879.10596 -17.553185 7.0098209 879.10596;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column1";
	rename -uid "E54BE2D4-FD40-69D8-3942-218733D5708D";
	setAttr ".rp" -type "double3" -578.86776997556763 179.50708359117328 898.60596284785959 ;
	setAttr ".sp" -type "double3" -578.86776997556763 179.50708359117328 898.60596284785959 ;
createNode mesh -n "Column1Shape" -p "Column1";
	rename -uid "01EFC95E-B347-6E71-8D0F-D79C024F747B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -598.3678 24.935089 918.10596 
		-559.3678 24.935089 918.10596 -598.3678 334.07907 918.10596 -559.3678 334.07907 918.10596 
		-598.3678 334.07907 879.10596 -559.3678 334.07907 879.10596 -598.3678 24.935089 879.10596 
		-559.3678 24.935089 879.10596;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Floor2";
	rename -uid "97160BB7-9144-DE0D-2C8E-6B9CD34671F6";
	setAttr ".rp" -type "double3" 807.73995049244377 179.30503985447359 -503.84773494821559 ;
	setAttr ".sp" -type "double3" 807.73995049244377 179.30503985447359 -503.84773494821559 ;
createNode mesh -n "FloorShape2" -p "Floor2";
	rename -uid "B3F60E27-8C40-AC49-B808-DD8E6F575DCB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  569.50067 167.37683 -262.47501 
		1045.9792 167.37683 -262.47501 569.50067 191.23325 -262.47501 1045.9792 191.23325 
		-262.47501 569.50067 191.23325 -745.22046 1045.9792 191.23325 -745.22046 569.50067 
		167.37683 -745.22046 1045.9792 167.37683 -745.22046;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window1";
	rename -uid "EEB9228E-6C48-546C-6C0D-D8B0F658E543";
	setAttr ".rp" -type "double3" -196.0508811891483 726.58096585459202 283.40813388028982 ;
	setAttr ".sp" -type "double3" -196.0508811891483 726.58096585459202 283.40813388028982 ;
createNode mesh -n "WindowShape1" -p "Window1";
	rename -uid "B4017A3A-FB42-689E-28B2-3C84453166F8";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "f[0:3]" "f[5:7]" "f[9:21]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 3 "f[4]" "f[8]" "f[10:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 34 ".uvst[0].uvsp[0:33]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25
		 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".pt[0:23]" -type "float3"  -216.12653 669.08887 373.85196 
		-175.97523 669.08887 373.85196 -216.12653 784.07318 373.85196 -175.97523 784.07318 
		373.85196 -216.12653 784.07318 192.96423 -175.97523 784.07318 192.96423 -216.12653 
		669.08887 192.96423 -175.97523 669.08887 192.96423 -216.12653 831.90326 283.40814 
		-216.12653 669.08887 283.40814 -175.97523 669.08887 283.40814 -175.97523 831.90326 
		283.40814 -175.97523 692.52362 283.40814 -175.97523 692.52362 347.8158 -175.97523 
		808.46851 283.40814 -175.97523 774.40729 347.8158 -175.97523 692.52362 219.00043 
		-175.97523 774.40729 219.00043 -187.16833 692.52362 283.40814 -187.16833 692.52362 
		347.8158 -187.16833 808.46851 283.40814 -187.16833 774.40729 347.8158 -187.16833 
		692.52362 219.00043 -187.16833 774.40729 219.00043;
	setAttr -s 24 ".vt[0:23]"  -0.5 -0.49999905 0.64700317 0.5 -0.49999905 0.64700317
		 -0.5 0.5 0.64700317 0.5 0.5 0.64700317 -0.5 0.5 -0.64700365 0.5 0.5 -0.64700365 -0.5 -0.49999905 -0.64700365
		 0.5 -0.49999905 -0.64700365 -0.5 0.91597033 0 -0.5 -0.49999905 0 0.5 -0.49999905 0
		 0.5 0.91597033 0 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963 0.5 0.71216202 0 0.5 0.41593742 0.46074963
		 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963
		 0.5 0.71216202 0 0.5 0.41593742 0.46074963 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998;
	setAttr -s 44 ".ed[0:43]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 11 8 1 10 12 0
		 1 13 0 12 13 0 11 14 0 3 15 0 15 14 0 13 15 0 7 16 0 16 12 0 5 17 0 17 16 0 14 17 0
		 12 18 0 13 19 0 18 19 0 14 20 0 18 20 1 15 21 0 21 20 0 19 21 0 16 22 0 22 18 0 17 23 0
		 23 22 0 20 23 0;
	setAttr -s 22 -ch 88 ".fc[0:21]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 18 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -34 35 -38 -39
		mu 0 4 28 29 30 31
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -36 -41 -43 -44
		mu 0 4 30 29 32 33
		f 4 -19 15 -3 -13
		mu 0 4 15 20 5 4
		f 4 -15 19 21 -21
		mu 0 4 1 19 23 22
		f 4 -8 23 24 -23
		mu 0 4 21 3 25 24
		f 4 -6 20 25 -24
		mu 0 4 3 1 22 25
		f 4 -12 26 27 -20
		mu 0 4 19 10 26 23
		f 4 -10 28 29 -27
		mu 0 4 10 11 27 26
		f 4 -16 22 30 -29
		mu 0 4 11 21 24 27
		f 4 -22 31 33 -33
		mu 0 4 22 23 29 28
		f 4 -25 36 37 -35
		mu 0 4 24 25 31 30
		f 4 -26 32 38 -37
		mu 0 4 25 22 28 31
		f 4 -28 39 40 -32
		mu 0 4 23 26 32 29
		f 4 -30 41 42 -40
		mu 0 4 26 27 33 32
		f 4 -31 34 43 -42
		mu 0 4 27 24 30 33;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window3";
	rename -uid "65B45D8F-7541-7F24-B2D8-048786E2B155";
	setAttr ".rp" -type "double3" -443.24370882842635 726.58096585459202 283.40813388028727 ;
	setAttr ".sp" -type "double3" -443.24370882842635 726.58096585459202 283.40813388028727 ;
createNode mesh -n "WindowShape3" -p "Window3";
	rename -uid "C8812A93-3E42-F7DB-A7BE-EC99681AC21E";
	setAttr -k off ".v";
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "f[0:3]" "f[5:7]" "f[9:21]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[8]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 3 "f[4]" "f[8]" "f[10:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 34 ".uvst[0].uvsp[0:33]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25
		 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.875 0 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".pt[0:23]" -type "float3"  -422.16806 669.08887 191.6703 
		-464.31934 669.08887 191.6703 -422.16806 784.07318 191.6703 -464.31934 784.07318 
		191.6703 -422.16806 784.07318 375.14603 -464.31934 784.07318 375.14603 -422.16806 
		669.08887 375.14603 -464.31934 669.08887 375.14603 -422.16806 831.90326 283.40814 
		-422.16806 669.08887 283.40814 -464.31934 669.08887 283.40814 -464.31934 831.90326 
		283.40814 -464.31934 692.52362 283.40814 -464.31934 692.52362 218.07898 -464.31934 
		808.46851 283.40814 -464.31934 774.40729 218.07898 -464.31934 692.52362 348.73734 
		-464.31934 774.40729 348.73734 -453.12628 692.52362 283.40814 -453.12628 692.52362 
		218.07898 -453.12628 808.46851 283.40814 -453.12628 774.40729 218.07898 -453.12628 
		692.52362 348.73734 -453.12628 774.40729 348.73734;
	setAttr -s 24 ".vt[0:23]"  -0.5 -0.49999905 0.64700317 0.5 -0.49999905 0.64700317
		 -0.5 0.5 0.64700317 0.5 0.5 0.64700317 -0.5 0.5 -0.64700365 0.5 0.5 -0.64700365 -0.5 -0.49999905 -0.64700365
		 0.5 -0.49999905 -0.64700365 -0.5 0.91597033 0 -0.5 -0.49999905 0 0.5 -0.49999905 0
		 0.5 0.91597033 0 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963 0.5 0.71216202 0 0.5 0.41593742 0.46074963
		 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998 0.5 -0.29619122 0 0.5 -0.29619122 0.46074963
		 0.5 0.71216202 0 0.5 0.41593742 0.46074963 0.5 -0.29619122 -0.46074998 0.5 0.41593742 -0.46074998;
	setAttr -s 44 ".ed[0:43]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 11 8 1 10 12 0
		 1 13 0 12 13 0 11 14 0 3 15 0 15 14 0 13 15 0 7 16 0 16 12 0 5 17 0 17 16 0 14 17 0
		 12 18 0 13 19 0 18 19 0 14 20 0 18 20 1 15 21 0 21 20 0 19 21 0 16 22 0 22 18 0 17 23 0
		 23 22 0 20 23 0;
	setAttr -s 22 -ch 88 ".fc[0:21]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 18 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -34 35 -38 -39
		mu 0 4 28 29 30 31
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -36 -41 -43 -44
		mu 0 4 30 29 32 33
		f 4 -19 15 -3 -13
		mu 0 4 15 20 5 4
		f 4 -15 19 21 -21
		mu 0 4 1 19 23 22
		f 4 -8 23 24 -23
		mu 0 4 21 3 25 24
		f 4 -6 20 25 -24
		mu 0 4 3 1 22 25
		f 4 -12 26 27 -20
		mu 0 4 19 10 26 23
		f 4 -10 28 29 -27
		mu 0 4 10 11 27 26
		f 4 -16 22 30 -29
		mu 0 4 11 21 24 27
		f 4 -22 31 33 -33
		mu 0 4 22 23 29 28
		f 4 -25 36 37 -35
		mu 0 4 24 25 31 30
		f 4 -26 32 38 -37
		mu 0 4 25 22 28 31
		f 4 -28 39 40 -32
		mu 0 4 23 26 32 29
		f 4 -30 41 42 -40
		mu 0 4 26 27 33 32
		f 4 -31 34 43 -42
		mu 0 4 27 24 30 33;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape6" -p "Window3";
	rename -uid "AD2A0207-4149-C983-8E5E-8089ED6EC906";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Cover2";
	rename -uid "073B1D2C-8547-5ABD-27A8-6AA54F080C7E";
	setAttr ".rp" -type "double3" 136.16348682856321 356.71615139943822 79.850821575805682 ;
	setAttr ".sp" -type "double3" 136.16348682856321 356.71615139943822 79.850821575805682 ;
createNode mesh -n "CoverShape2" -p "Cover2";
	rename -uid "3A20E702-5E48-92D2-997E-54B14032D7D3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  241.55231 302.14221 194.25917 
		245.1761 300.94055 -37.571774 251.06747 316.08115 194.14478 254.69124 314.87939 -37.686043 
		27.150875 412.49173 197.27342 30.774679 411.2901 -34.557533 17.63574 398.55289 197.38768 
		21.259487 397.35117 -34.44313;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window4";
	rename -uid "4A7CAC57-F244-9AAD-E829-BC8EAE068AC4";
	setAttr ".t" -type "double3" -335.62290481607363 531.8481392229312 648.18874605415624 ;
	setAttr ".r" -type "double3" 0 270 0 ;
	setAttr ".s" -type "double3" 41.151281224598755 115.98442169280271 140.78885205848553 ;
createNode mesh -n "polySurfaceShape6" -p "Window4";
	rename -uid "0D067D7D-1640-AAA9-F6FD-9C809010A6A9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -4.6629367e-15 3.1086245e-15 
		0.14700346 -1.5543122e-15 0 0.14700346 -4.6629367e-15 0 0.14700346 -1.5543122e-15 
		-3.1086245e-15 0.14700346 -4.6629367e-15 0 -0.14700346 -1.5543122e-15 -3.1086245e-15 
		-0.14700346 -4.6629367e-15 3.1086245e-15 -0.14700346 -1.5543122e-15 0 -0.14700346 
		-3.1086245e-15 0.41597036 4.5770526e-15 -4.6629367e-15 3.1086245e-15 4.5770526e-15 
		-1.5543122e-15 0 2.4093888e-15 0 0.41597036 2.4093888e-15 0 0 -2.1175824e-22 0 0 
		4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface11" -p "Window4";
	rename -uid "439147EE-054F-950F-710F-7B9DC798A676";
	setAttr ".rp" -type "double3" 0.096429993656757418 -1.7763568394002505e-15 -4.4408920985006262e-16 ;
	setAttr ".sp" -type "double3" 0.096429993656757418 -1.7763568394002505e-15 -4.4408920985006262e-16 ;
createNode mesh -n "polySurfaceShape36" -p "polySurface11";
	rename -uid "87363CD6-FC49-0A54-9A72-3F83C25887DA";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "f[0:3]" "f[5:7]" "f[9:21]";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "f[5]" "f[6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 14 "f[4]" "f[8]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.8125 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 34 ".uvst[0].uvsp[0:33]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.375 0.375 0.375 0.375 0.5 0.625 0.5 0.625 0.75 0.375 0.75
		 0.375 0.875 0.625 0.875 0.625 1 0.375 1 0.625 0 0.75 0 0.75 0.25 0.625 0.25 0.25
		 0.25 0.25 0 0.125 0 0.125 0.25 0.875 0 0.875 0.25 0.75 0 0.75 0 0.625 0 0.75 0.25
		 0.625 0.25 0.75 0.25 0.875 0 0.875 0 0.875 0.25 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".pt[0:23]" -type "float3"  0.018964738 -1.9428903e-15 
		-4.4408921e-16 0.17389511 -1.9428903e-15 -4.4408921e-16 0.018964738 -1.9428903e-15 
		-4.4408921e-16 0.17389511 -1.9428903e-15 -4.4408921e-16 0.018964738 -1.9428903e-15 
		-4.4408921e-16 0.17389511 -1.9428903e-15 -4.4408921e-16 0.018964738 -1.9428903e-15 
		-4.4408921e-16 0.17389511 -1.9428903e-15 -4.4408921e-16 0.018964738 -1.9984014e-15 
		-4.1000789e-16 0.018964738 -1.9428903e-15 -4.1000789e-16 0.17389511 -1.9428903e-15 
		-4.1000789e-16 0.17389511 -1.9984014e-15 -4.1000789e-16 0.17389511 -1.9428903e-15 
		-4.1000789e-16 0.17389511 -1.9428903e-15 -3.8857806e-16 0.17389511 -1.9984014e-15 
		-4.1000789e-16 0.17389511 -1.9428903e-15 -3.8857806e-16 0.17389511 -1.9428903e-15 
		-3.8857806e-16 0.17389511 -1.9428903e-15 -3.8857806e-16 0.1317544 -1.9428903e-15 
		-4.1000789e-16 0.1317544 -1.9428903e-15 -3.8857806e-16 0.1317544 -1.9984014e-15 -4.1000789e-16 
		0.1317544 -1.9428903e-15 -3.8857806e-16 0.1317544 -1.9428903e-15 -3.8857806e-16 0.1317544 
		-1.9428903e-15 -3.8857806e-16;
	setAttr -s 24 ".vt[0:23]"  -0.5 -0.49999905 0.64700317 0.49999905 -0.49999905 0.64700317
		 -0.5 0.49999952 0.64700317 0.49999905 0.49999952 0.64700317 -0.5 0.49999952 -0.64700377
		 0.49999905 0.49999952 -0.64700377 -0.5 -0.49999905 -0.64700377 0.49999905 -0.49999905 -0.64700377
		 -0.5 0.91597033 -2.3841858e-07 -0.5 -0.49999905 -2.3841858e-07 0.49999905 -0.49999905 -2.3841858e-07
		 0.49999905 0.91597033 -2.3841858e-07 0.49999905 -0.29619122 -2.3841858e-07 0.49999905 -0.29619122 0.46074939
		 0.49999905 0.71216202 -2.3841858e-07 0.49999905 0.41593695 0.46074939 0.49999905 -0.29619122 -0.46075022
		 0.49999905 0.41593695 -0.46075022 0.22800159 -0.29619122 -2.3841858e-07 0.22800159 -0.29619122 0.46074939
		 0.22800159 0.71216202 -2.3841858e-07 0.22800159 0.41593695 0.46074939 0.22800159 -0.29619122 -0.46075022
		 0.22800159 0.41593695 -0.46075022;
	setAttr -s 44 ".ed[0:43]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 11 8 1 10 12 0
		 1 13 0 12 13 0 11 14 0 3 15 0 15 14 0 13 15 0 7 16 0 16 12 0 5 17 0 17 16 0 14 17 0
		 12 18 0 13 19 0 18 19 0 14 20 0 18 20 1 15 21 0 21 20 0 19 21 0 16 22 0 22 18 0 17 23 0
		 23 22 0 20 23 0;
	setAttr -s 22 -ch 88 ".fc[0:21]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 18 -7
		mu 0 4 3 2 4 5
		f 4 2 9 -4 -9
		mu 0 4 6 7 8 9
		f 4 17 14 -1 -14
		mu 0 4 10 11 12 13
		f 4 -34 35 -38 -39
		mu 0 4 14 15 16 17
		f 4 16 13 4 6
		mu 0 4 18 19 0 3
		f 4 10 -17 12 8
		mu 0 4 20 19 18 21
		f 4 3 11 -18 -11
		mu 0 4 9 8 11 10
		f 4 -36 -41 -43 -44
		mu 0 4 16 15 22 23
		f 4 -19 15 -3 -13
		mu 0 4 5 4 7 6
		f 4 -15 19 21 -21
		mu 0 4 1 24 25 26
		f 4 -8 23 24 -23
		mu 0 4 27 2 28 29
		f 4 -6 20 25 -24
		mu 0 4 2 1 26 28
		f 4 -12 26 27 -20
		mu 0 4 24 30 31 25
		f 4 -10 28 29 -27
		mu 0 4 30 32 33 31
		f 4 -16 22 30 -29
		mu 0 4 32 27 29 33
		f 4 -22 31 33 -33
		mu 0 4 26 25 15 14
		f 4 -25 36 37 -35
		mu 0 4 29 28 17 16
		f 4 -26 32 38 -37
		mu 0 4 28 26 14 17
		f 4 -28 39 40 -32
		mu 0 4 25 31 22 15
		f 4 -30 41 42 -40
		mu 0 4 31 33 23 22
		f 4 -31 34 43 -42
		mu 0 4 33 29 16 23;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface12" -p "Window4";
	rename -uid "DA091F69-4F47-92CA-DD88-1480DB57E073";
createNode mesh -n "polySurfaceShape37" -p "polySurface12";
	rename -uid "3CABFCD3-5947-2B9E-F688-C9A76324B225";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[0]" "f[1]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 6 ".uvst[0].uvsp[0:5]" -type "float2" 0.625 0 0.75 0 0.75
		 0.25 0.625 0.25 0.875 0 0.875 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".vt[0:5]"  0.22800159 -0.29619122 0.46074939 0.22800159 -0.29619122 -2.3841858e-07
		 0.22800159 0.71216202 -2.3841858e-07 0.22800159 0.41593695 0.46074939 0.22800159 -0.29619122 -0.46075022
		 0.22800159 0.41593695 -0.46075022;
	setAttr -s 7 ".ed[0:6]"  1 0 0 1 2 1 3 2 0 0 3 0 4 1 0 5 4 0 2 5 0;
	setAttr -s 2 -ch 8 ".fc[0:1]" -type "polyFaces" 
		f 4 -1 1 -3 -4
		mu 0 4 0 1 2 3
		f 4 -2 -5 -6 -7
		mu 0 4 2 1 4 5;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Step";
	rename -uid "4DEBEAA3-CB4C-3EC0-26E2-B5BC104B9DAA";
	setAttr ".rp" -type "double3" 73.432679522806325 52.828081290413472 -20.549132950606861 ;
	setAttr ".sp" -type "double3" 73.432679522806325 52.828081290413472 -20.549132950606861 ;
createNode mesh -n "StepShape" -p "Step";
	rename -uid "762B8185-A349-553C-708E-3690AC318168";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  73.932686 53.328075 182.01051 
		206.21173 53.328075 182.01051 73.932686 76.242279 182.01051 206.21173 76.242279 182.01051 
		73.932686 76.242279 -20.049135 206.21173 76.242279 -20.049135 73.932686 53.328075 
		-20.049135 206.21173 53.328075 -20.049135;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder4";
	rename -uid "3599F7BA-324C-EB8E-7836-01A4693B0B92";
	setAttr ".rp" -type "double3" 745.24474945182487 70.109793686902691 -283.273918280673 ;
	setAttr ".sp" -type "double3" 745.24474945182487 70.109793686902691 -283.273918280673 ;
createNode mesh -n "ladderShape4" -p "ladder4";
	rename -uid "BACC00F0-0647-2F4D-08A0-34B3F53F7B02";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  745.74475 70.609795 -241.74802 
		860.87415 70.609795 -241.74802 745.74475 83.657227 -241.74802 860.87415 83.657227 
		-241.74802 745.74475 83.657227 -282.77393 860.87415 83.657227 -282.77393 745.74475 
		70.609795 -282.77393 860.87415 70.609795 -282.77393;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window2";
	rename -uid "AC664C86-A949-F1B7-9D33-21857D2213D1";
	setAttr ".rp" -type "double3" 83.080806411241255 291.0765633193248 423.29270593068026 ;
	setAttr ".sp" -type "double3" 83.080806411241255 291.0765633193248 423.29270593068026 ;
createNode mesh -n "WindowShape2" -p "Window2";
	rename -uid "8970412E-7E4C-DB7B-CACE-69AAFF3091CB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  64.236778 205.46945 498.56689 
		104.13503 205.46945 500.77722 64.236778 376.68369 498.56689 104.13503 376.68369 500.77722 
		62.026466 376.68369 345.80807 101.92472 376.68369 348.0184 62.026466 205.46945 345.80807 
		101.92472 205.46945 348.0184 102.35239 229.9588 369.86584 103.7073 229.9588 478.92987 
		102.35239 352.19434 369.86584 103.7073 352.19434 478.92987 75.215225 229.9588 370.39709 
		76.570137 229.9588 479.46115 75.215225 352.19434 370.39709 76.570137 352.19434 479.46115;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Door";
	rename -uid "93AA170C-6E44-63EA-20DE-E49046532BBE";
	setAttr ".rp" -type "double3" 85.283565923392615 162.89947746913111 83.555715375205224 ;
	setAttr ".sp" -type "double3" 85.283565923392615 162.89947746913111 83.555715375205224 ;
createNode mesh -n "DoorShape" -p "Door";
	rename -uid "1FAF6222-7045-B940-8ECA-CE92324B555D";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  73.906891 16.179594 151.4267 
		98.872986 16.179594 153.63953 73.906891 309.61935 151.4267 98.872986 309.61935 153.63953 
		71.694069 309.61935 13.471804 96.660172 309.61935 15.684625 71.694069 16.179594 13.471804 
		96.660172 16.179594 15.684625 97.04641 58.151302 35.415436 98.486702 58.151302 133.90881 
		97.04641 267.64764 35.415436 98.486702 267.64764 133.90881 79.99295 58.151302 35.749287 
		81.433243 58.151302 134.24266 79.99295 267.64764 35.749287 81.433243 267.64764 134.24266;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof1";
	rename -uid "0A1ADE53-B049-C42A-DB17-AB91551EB10C";
	setAttr ".rp" -type "double3" -337.3568115234375 722.48785400390625 630.9681396484375 ;
	setAttr ".sp" -type "double3" -337.3568115234375 722.48785400390625 630.9681396484375 ;
createNode mesh -n "Roof1Shape" -p "Roof1";
	rename -uid "78262366-B544-0470-A384-2BBAF81E6928";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -757.50665 379.89539 751.42596 
		59.315575 375.57306 737.99988 -859.24841 435.89926 753.4151 180.53984 386.90646 735.62982 
		-847.52429 435.89926 639.88788 192.26401 386.90646 622.1026 -745.78253 379.89539 
		637.89874 71.039734 375.57306 624.47266 -349.09549 721.1944 744.71295 -349.09549 
		783.69104 744.71295 -337.37143 783.69104 631.18579 -337.37143 721.1944 631.18579;
	setAttr -s 12 ".vt[0:11]"  -1.031519413 0.20975935 0.24336851 1.0023268461 0.19603533 0.19189304
		 -1.28485131 0.38758004 0.2509948 1.30416918 0.23202056 0.18280631 -1.25565875 0.38758004 -0.18426687
		 1.33336174 0.23202056 -0.25245535 -1.0023268461 0.20975935 -0.19189316 1.031519413 0.19603533 -0.24336863
		 -0.01459618 1.2934351 0.21763074 -0.01459618 1.49187136 0.21763074 0.01459618 1.49187136 -0.21763074
		 0.01459618 1.2934351 -0.21763074;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 1 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 0 10 11 1 11 8 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof3";
	rename -uid "B9EB99EE-1C4B-A249-5C25-9994001C9728";
	setAttr ".rp" -type "double3" -189.26956505276166 703.49857228015583 281.56564607866164 ;
	setAttr ".sp" -type "double3" -189.26956505276166 703.49857228015583 281.56564607866164 ;
createNode mesh -n "Roof3Shape" -p "Roof3";
	rename -uid "AE2D2B35-6442-D210-F452-A198BAB4AD40";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -233.70436 706.42456 434.65244 
		-141.13419 706.42456 437.51224 -231.99493 719.7948 521.97107 -139.42474 719.7948 
		524.83063 -239.09178 721.12366 39.455227 -146.52159 721.12366 42.314823 -237.40494 
		727.50323 125.61907 -144.83476 727.50323 128.47888 -235.55464 922.50873 280.13574 
		-235.55464 863.8371 280.13574 -142.9845 863.8371 282.99554 -142.9845 922.50873 282.99554 
		0 0 -2.1175824e-22 0 0 4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof4";
	rename -uid "C44D2A57-7644-1556-C57C-D284AEA9FB5C";
	setAttr ".rp" -type "double3" -456.65705694330245 703.49857228015583 286.80029213068474 ;
	setAttr ".sp" -type "double3" -456.65705694330245 703.49857228015583 286.80029213068474 ;
createNode mesh -n "Roof4Shape" -p "Roof4";
	rename -uid "F4A67CEC-1649-7729-F97A-4FB3C50E4E3F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[7]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5:6]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[8]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[9]";
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.25 0.25 0.375 0.375 0.25 0 0.375 0.875 0.625 0.875
		 0.75 0 0.625 0.375 0.75 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 2 ".clst";
	setAttr ".clst[0].clsn" -type "string" "SculptFreezeColorTemp";
	setAttr ".clst[1].clsn" -type "string" "SculptMaskColorTemp";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -501.09186 706.42456 439.88708 
		-408.52167 706.42456 442.74689 -499.38242 719.7948 527.20575 -406.81223 719.7948 
		530.06525 -506.47928 721.12366 44.689873 -413.90909 721.12366 47.549469 -504.79242 
		727.50323 130.85371 -412.22226 727.50323 133.71353 -502.94217 922.50879 285.37039 
		-502.94214 863.8371 285.37039 -410.37198 863.8371 288.23019 -410.37198 922.50873 
		288.23019 0 0 -2.1175824e-22 0 0 4.2351647e-22 0 0 0 0 0 0;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 0.5 0 -0.5 -0.5 0 0.5 -0.5 0
		 0.5 0.5 0;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 8 0
		 3 11 0 4 6 0 5 7 0 6 9 0 7 10 0 8 4 0 9 0 0 10 1 0 11 5 0 8 9 1 9 10 1 10 11 1 11 8 1;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 19 -7
		mu 0 4 2 3 20 15
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 17 14 -1 -14
		mu 0 4 17 18 9 8
		f 4 -15 18 -8 -6
		mu 0 4 1 19 21 3
		f 4 16 13 4 6
		mu 0 4 14 16 0 2
		f 4 10 -17 12 8
		mu 0 4 12 16 14 13
		f 4 3 11 -18 -11
		mu 0 4 6 7 18 17
		f 4 -19 -12 -10 -16
		mu 0 4 21 19 10 11
		f 4 -20 15 -3 -13
		mu 0 4 15 20 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".cfsc" yes;
createNode transform -n "Step1";
	rename -uid "57E28EC4-D449-4FFB-8795-838D13E908AA";
	setAttr ".rp" -type "double3" 955.23284892678441 478.61745846431069 -305.27698429791099 ;
	setAttr ".sp" -type "double3" 955.23284892678441 478.61745846431069 -305.27698429791099 ;
createNode mesh -n "Step1Shape" -p "Step1";
	rename -uid "49DE8289-9B41-4E45-514D-CE997DFB90DE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  945.0611 469.00128 -244.7614 
		965.4046 469.00128 -244.7614 945.0611 488.23364 -244.7614 965.4046 488.23364 -244.7614 
		945.0611 488.23364 -365.79257 965.4046 488.23364 -365.79257 945.0611 469.00128 -365.79257 
		965.4046 469.00128 -365.79257;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Step2";
	rename -uid "60A552A5-AD45-734B-2277-04A0AD5DDCF0";
	setAttr ".rp" -type "double3" -47.56956043227305 96.956596380033716 661.04570395563462 ;
	setAttr ".sp" -type "double3" -47.56956043227305 96.956596380033716 661.04570395563462 ;
createNode mesh -n "Step2Shape" -p "Step2";
	rename -uid "F0125C33-8E42-7B4A-5A39-389DBAA52325";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  42.687359 87.340904 727.18524 
		41.687359 87.340904 593.90619 42.687359 106.57229 727.18524 41.687359 106.57229 593.90619 
		-136.82648 106.57229 728.18524 -137.82648 106.57229 594.90619 -136.82648 87.340904 
		728.18524 -137.82648 87.340904 594.90619;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Door1";
	rename -uid "5A6E5D26-D841-B3CF-12BC-7C8AB6215DCE";
	setAttr ".rp" -type "double3" -336.35417482692515 157.83298563845221 649.05171061045417 ;
	setAttr ".sp" -type "double3" -336.35417482692515 157.83298563845221 649.05171061045417 ;
createNode mesh -n "Door1Shape" -p "Door1";
	rename -uid "C4EBB68B-1945-C95D-D245-27ABA9E2FF28";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -404.69299 11.113103 636.89471 
		-407.25494 11.113103 662.17328 -404.69299 304.55289 636.89471 -407.25494 304.55289 
		662.17328 -265.45331 304.55289 635.93005 -268.01523 304.55289 661.20862 -265.45331 
		11.113103 635.93005 -268.01523 11.113103 661.20862 -287.93073 53.084812 661.4173 
		-387.33954 53.084812 661.9646 -287.93073 262.58115 661.4173 -387.33954 262.58115 
		661.9646 -288.26456 53.084812 644.36383 -387.6734 53.084812 644.91113 -288.26456 
		262.58115 644.36383 -387.6734 262.58115 644.91113;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window5";
	rename -uid "A3BBBE5B-F34F-2E12-BFFF-908D2D0562B6";
	setAttr ".rp" -type "double3" -163.70149336407701 215.64291692700039 648.6717178754069 ;
	setAttr ".sp" -type "double3" -163.70149336407701 215.64291692700039 648.6717178754069 ;
createNode mesh -n "WindowShape5" -p "Window5";
	rename -uid "7878BE5F-1A42-37FA-FCAA-0F8F5FBC6CD6";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -239.44354 130.0358 629.04736 
		-242.00298 130.0358 669.25806 -239.44354 301.25003 629.04736 -242.00298 301.25003 
		669.25806 -85.399902 301.25003 628.08521 -87.959328 301.25003 668.29596 -85.399902 
		130.0358 628.08521 -87.959328 130.0358 668.29596 -109.99146 154.52515 668.54602 -219.97095 
		154.52515 669.00793 -109.99146 276.76068 668.54602 -219.97095 276.76068 669.00793 
		-110.52272 154.52515 641.40887 -220.50223 154.52515 641.87079 -110.52272 276.76068 
		641.40887 -220.50223 276.76068 641.87079;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window6";
	rename -uid "74439F2E-084E-D3FE-7BB4-54BDE219EC26";
	setAttr ".rp" -type "double3" -560.8217555152587 215.64291692700039 643.99590625663575 ;
	setAttr ".sp" -type "double3" -560.8217555152587 215.64291692700039 643.99590625663575 ;
createNode mesh -n "WindowShape6" -p "Window6";
	rename -uid "E343582B-E940-6F08-20E9-CAA46D44A926";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -636.56378 130.0358 624.37158 
		-639.12323 130.0358 664.58228 -636.56378 301.25003 624.37158 -639.12323 301.25003 
		664.58228 -482.52017 301.25003 623.40942 -485.07959 301.25003 663.62012 -482.52017 
		130.0358 623.40942 -485.07959 130.0358 663.62012 -507.11172 154.52515 663.87024 -617.09119 
		154.52515 664.33215 -507.11172 276.76068 663.87024 -617.09119 276.76068 664.33215 
		-507.64297 154.52515 636.73309 -617.6225 154.52515 637.19501 -507.64297 276.76068 
		636.73309 -617.6225 276.76068 637.19501;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window8";
	rename -uid "E58CABC7-AC49-C32C-1D47-FF829B165792";
	setAttr ".rp" -type "double3" -760.58898170408031 268.49475525544744 130.2018896155742 ;
	setAttr ".sp" -type "double3" -760.58898170408031 268.49475525544744 130.2018896155742 ;
createNode mesh -n "WindowShape8" -p "Window8";
	rename -uid "F9045C27-9F4F-8819-7220-B5A4479C9492";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -741.4325 152.24886 18.345106 
		-781.99231 152.24886 15.060604 -741.4325 384.74066 18.345106 -781.99231 384.74066 
		15.060604 -739.18555 384.74066 245.34335 -779.74536 384.74066 242.05884 -739.18555 
		152.24886 245.34335 -779.74536 152.24886 242.05884 -780.18011 185.50293 209.59372 
		-781.5575 185.50293 47.525551 -780.18011 351.48657 209.59372 -781.5575 351.48657 
		47.525551 -753.04297 185.50293 208.81512 -754.42035 185.50293 46.746956 -753.04297 
		351.48657 208.81512 -754.42035 351.48657 46.746956;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Window9";
	rename -uid "A927EA79-E44F-9109-4F5A-B78AF9BEE298";
	setAttr ".rp" -type "double3" -758.35992178994354 268.49475525544744 441.36903129792955 ;
	setAttr ".sp" -type "double3" -758.35992178994354 268.49475525544744 441.36903129792955 ;
createNode mesh -n "WindowShape9" -p "Window9";
	rename -uid "741859CE-2B42-1B58-3FDD-279AF8DD2B10";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 2 "f[0:3]" "f[5:13]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 1 "f[4]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[6:13]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0 0.875 0 0.875 0.25 0.625 0.25 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[0:15]" -type "float3"  -739.20343 152.24886 329.51224 
		-779.76324 152.24886 326.22775 -739.20343 384.74066 329.51224 -779.76324 384.74066 
		326.22775 -736.95648 384.74066 556.5105 -777.5163 384.74066 553.22601 -736.95648 
		152.24886 556.5105 -777.5163 152.24886 553.22601 -777.95105 185.50293 520.76086 -779.32843 
		185.50293 358.69269 -777.95105 351.48657 520.76086 -779.32843 351.48657 358.69269 
		-750.8139 185.50293 519.9823 -752.19128 185.50293 357.91409 -750.8139 351.48657 519.9823 
		-752.19128 351.48657 357.91409;
	setAttr -s 16 ".vt[0:15]"  -0.15623283 -0.70975494 0.62408638 0.17455721 -0.70975494 0.64241171
		 -0.15623283 0.70975494 0.62408638 0.17455721 0.70975494 0.64241171 -0.17455816 0.70975494 -0.64241266
		 0.15623188 0.70975494 -0.62408733 -0.17455816 -0.70975494 -0.64241266 0.15623188 -0.70975494 -0.62408733
		 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892 0.15977764 0.50671768 -0.44295359
		 0.17101097 0.50671768 0.46127892 0.15977764 -0.50671768 -0.44295359 0.17101097 -0.50671768 0.46127892
		 0.15977764 0.50671768 -0.44295359 0.17101097 0.50671768 0.46127892;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 7 8 0 1 9 0 8 9 0 5 10 0 10 8 0 3 11 0 11 10 0 9 11 0
		 8 12 0 9 13 0 12 13 0 10 14 0 14 12 0 11 15 0 15 14 0 13 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -23 -25 -27 -28
		mu 0 4 18 19 20 21
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -12 12 14 -14
		mu 0 4 1 10 15 14
		f 4 -10 15 16 -13
		mu 0 4 10 11 16 15
		f 4 -8 17 18 -16
		mu 0 4 11 3 17 16
		f 4 -6 13 19 -18
		mu 0 4 3 1 14 17
		f 4 -15 20 22 -22
		mu 0 4 14 15 19 18
		f 4 -17 23 24 -21
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 21 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column3";
	rename -uid "91A098E9-8141-F548-0A5E-9EB0CC7E8B1F";
	setAttr ".rp" -type "double3" 635.87922914051921 232.37354291627059 -674.5772594785974 ;
	setAttr ".sp" -type "double3" 635.87922914051921 232.37354291627059 -674.5772594785974 ;
createNode mesh -n "Column3Shape" -p "Column3";
	rename -uid "F0D787D3-5A4D-223E-BA23-1384AD7CF497";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  655.71161 13.688342 -694.40967 
		635.87921 13.688342 -702.62451 616.04681 13.688342 -694.40967 607.83197 13.688342 
		-674.57727 616.04681 13.688342 -654.74487 635.87921 13.688342 -646.53003 655.71161 
		13.688342 -654.74487 663.92645 13.688342 -674.57727 655.71161 451.05875 -694.40967 
		635.87921 451.05875 -702.62451 616.04681 451.05875 -694.40967 607.83197 451.05875 
		-674.57727 616.04681 451.05875 -654.74487 635.87921 451.05875 -646.53003 655.71161 
		451.05875 -654.74487 663.92645 451.05875 -674.57727 635.87921 13.688342 -674.57727 
		635.87921 451.05875 -674.57727;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column4";
	rename -uid "26A4C17B-7446-A928-114F-F288BDFF4FC8";
	setAttr ".rp" -type "double3" 971.11138737936403 229.53664739735308 -334.79941022930439 ;
	setAttr ".sp" -type "double3" 971.11138737936403 229.53664739735308 -334.79941022930439 ;
createNode mesh -n "Column4Shape" -p "Column4";
	rename -uid "90973F3A-6946-783D-98AF-248215695113";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  990.94379 10.851446 -354.63181 
		971.11139 10.851446 -362.84665 951.27899 10.851446 -354.63181 943.06415 10.851446 
		-334.79941 951.27899 10.851446 -314.96701 971.11139 10.851446 -306.75217 990.94379 
		10.851446 -314.96701 999.15863 10.851446 -334.79941 990.94379 448.22186 -354.63181 
		971.11139 448.22186 -362.84665 951.27899 448.22186 -354.63181 943.06415 448.22186 
		-334.79941 951.27899 448.22186 -314.96701 971.11139 448.22186 -306.75217 990.94379 
		448.22186 -314.96701 999.15863 448.22186 -334.79941 971.11139 10.851446 -334.79941 
		971.11139 448.22186 -334.79941;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column5";
	rename -uid "19983BCF-C14C-F0C2-DF26-069C141E3795";
	setAttr ".rp" -type "double3" 639.26987224280515 221.53875143166931 -335.09513982983287 ;
	setAttr ".sp" -type "double3" 639.26987224280515 221.53875143166931 -335.09513982983287 ;
createNode mesh -n "Column5Shape" -p "Column5";
	rename -uid "75658C3B-2247-0524-7381-4ABD90BBC9CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  659.10223 2.8535507 -354.92752 
		639.2699 2.8535507 -363.14236 619.4375 2.8535507 -354.92752 611.22266 2.8535507 -335.09515 
		619.4375 2.8535507 -315.26276 639.2699 2.8535507 -307.04791 659.10223 2.8535507 -315.26276 
		667.31714 2.8535507 -335.09515 659.10223 440.22394 -354.92752 639.2699 440.22394 
		-363.14236 619.4375 440.22394 -354.92752 611.22266 440.22394 -335.09515 619.4375 
		440.22394 -315.26276 639.2699 440.22394 -307.04791 659.10223 440.22394 -315.26276 
		667.31714 440.22394 -335.09515 639.2699 2.8535507 -335.09515 639.2699 440.22394 -335.09515;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Column6";
	rename -uid "2F3F8B6E-8C41-0102-E280-49ADE7D9D37C";
	setAttr ".rp" -type "double3" 969.9119290449438 225.53354182797827 -680.880027909315 ;
	setAttr ".sp" -type "double3" 969.9119290449438 225.53354182797827 -680.880027909315 ;
createNode mesh -n "Column6Shape" -p "Column6";
	rename -uid "8DBEEC0D-8749-59B0-1050-37861D905035";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  989.74432 6.848341 -700.7124 
		969.91193 6.848341 -708.92725 950.07953 6.848341 -700.7124 941.86469 6.848341 -680.88 
		950.07953 6.848341 -661.04767 969.91193 6.848341 -652.83276 989.74432 6.848341 -661.04761 
		997.95917 6.848341 -680.88 989.74432 444.21875 -700.7124 969.91193 444.21875 -708.92725 
		950.07953 444.21875 -700.7124 941.86469 444.21875 -680.88 950.07953 444.21875 -661.04767 
		969.91193 444.21875 -652.83276 989.74432 444.21875 -661.04761 997.95917 444.21875 
		-680.88 969.91193 6.848341 -680.88 969.91193 444.21875 -680.88;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof2";
	rename -uid "CF57B106-F141-1711-53C5-63AA412C01EE";
	setAttr ".rp" -type "double3" -337.3568115234375 722.48785400390625 -83.355194091796861 ;
	setAttr ".sp" -type "double3" -337.3568115234375 722.48785400390625 -83.355194091796861 ;
createNode mesh -n "Roof2Shape" -p "Roof2";
	rename -uid "4184B210-094F-F343-E28F-6E9503D62992";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -745.75336 379.89539 -76.859772 
		71.068871 375.57306 -90.285873 -847.49512 435.89923 -74.870644 192.29314 386.90646 
		-92.655922 -835.771 435.89923 -188.39786 204.0173 386.90646 -206.18314 -734.02924 
		379.89539 -190.38698 82.79303 375.57306 -203.81308 -337.34219 721.1944 -83.57283 
		-337.34219 783.69104 -83.57283 -325.61813 783.69104 -197.09999 -325.61813 721.1944 
		-197.09999;
	setAttr -s 12 ".vt[0:11]"  -1.031519413 0.20975935 0.24336851 1.0023268461 0.19603533 0.19189304
		 -1.28485131 0.38758004 0.2509948 1.30416918 0.23202056 0.18280631 -1.25565875 0.38758004 -0.18426687
		 1.33336174 0.23202056 -0.25245535 -1.0023268461 0.20975935 -0.19189316 1.031519413 0.19603533 -0.24336863
		 -0.01459618 1.2934351 0.21763074 -0.01459618 1.49187136 0.21763074 0.01459618 1.49187136 -0.21763074
		 0.01459618 1.2934351 -0.21763074;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 1 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 0 10 11 1 11 8 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 14 16 2
		f 4 1 17 -3 -7
		mu 0 4 2 16 17 4
		f 4 2 18 -4 -9
		mu 0 4 4 17 18 6
		f 4 3 19 -1 -11
		mu 0 4 6 18 15 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 -17 12 5 -14
		mu 0 4 16 14 1 3
		f 4 -18 13 7 -15
		mu 0 4 17 16 3 5
		f 4 -19 14 9 -16
		mu 0 4 18 17 5 7
		f 4 -20 15 11 -13
		mu 0 4 15 18 7 9;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof6";
	rename -uid "FD723533-254C-591B-580C-BFAB609541F1";
	setAttr ".t" -type "double3" 813.67110411627834 378.43480696230392 -214.27287874635817 ;
	setAttr ".s" -type "double3" 714.10337037605029 1058.3151515124987 134.93934932269772 ;
	setAttr ".rp" -type "double3" -2.2573309602094913e-05 382.59497819394608 -67.469674964579326 ;
	setAttr ".sp" -type "double3" -3.1610703388196271e-08 0.36151327668998989 -0.50000000224716667 ;
	setAttr ".spt" -type "double3" -2.2541698898706717e-05 382.23346491725607 -66.969674962332164 ;
createNode mesh -n "polySurfaceShape8" -p "Roof6";
	rename -uid "F1E7753F-F14B-1141-7A09-02A5F5AC705B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof7";
	rename -uid "FA71EB3D-6C44-88FB-90DC-12850980549E";
	setAttr ".t" -type "double3" 813.67110411627846 378.43480696230381 -775.12357985245842 ;
	setAttr ".s" -type "double3" 714.10337037605029 1058.3151515124987 134.93934932269772 ;
	setAttr ".rp" -type "double3" -2.2573309760657715e-05 382.59497819394619 67.469649188395948 ;
	setAttr ".sp" -type "double3" -3.1610703610240876e-08 0.36151327668999 0.49999981122664572 ;
	setAttr ".spt" -type "double3" -2.2541699057047474e-05 382.23346491725619 66.969649377169304 ;
createNode mesh -n "polySurfaceShape8" -p "Roof7";
	rename -uid "8CB4F458-554B-224F-8CF2-D2A7FCDED18B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood5";
	rename -uid "365A27FB-5D4C-4E34-9901-1B8D6F2A3B06";
	setAttr ".rp" -type "double3" 807.03801219992511 449.73868002597999 -335.92290269517372 ;
	setAttr ".sp" -type "double3" 807.03801219992511 449.73868002597999 -335.92290269517372 ;
createNode mesh -n "Wood5Shape" -p "Wood5";
	rename -uid "AB76B3BE-C545-6C64-3F60-96AB6F79176B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  1017.4532 430.23868 -297.4205 
		1016.4532 470.23868 -297.4205 597.62286 429.23868 -297.4205 596.62286 469.23868 -297.4205 
		597.62286 429.23868 -374.42529 596.62286 469.23868 -374.42529 1017.4532 430.23868 
		-374.42529 1016.4532 470.23868 -374.42529;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Roof8";
	rename -uid "326BE4DF-CB40-C467-71D2-1F972FE7CF95";
	setAttr ".t" -type "double3" -0.5156314045052568 -75.782271999692853 -9.8152583576208485 ;
	setAttr ".s" -type "double3" 1 1 0.63001666562719316 ;
	setAttr ".rp" -type "double3" 813.67110411627868 593.36502782422008 -494.69822929940761 ;
	setAttr ".sp" -type "double3" 813.67110411627868 593.36502782422008 -494.69822929940761 ;
createNode transform -n "polySurface8" -p "Roof8";
	rename -uid "AD57F12C-BC45-663D-DC2F-9EBF3567F16C";
	setAttr ".rp" -type "double3" 813.67108154296875 593.36503601074219 -214.27287292480457 ;
	setAttr ".sp" -type "double3" 813.67108154296875 593.36503601074219 -214.27287292480457 ;
createNode mesh -n "polySurfaceShape29" -p "polySurface8";
	rename -uid "904AB9E6-AC41-CEDF-FC5C-6FA1398DD2B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 7 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "rim";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[6].gtagnm" -type "string" "top";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.5 0 0.5
		 0.25 0.375 0.25 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75 0.5 1 0.375 1 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25 0.125 0 0.125 0.25 0.625 0.5 0.625 0.75 0.625 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  456.61938477 364.5562439 -146.80319214 1170.72277832 364.5562439 -146.80319214
		 456.61938477 425.70022583 -146.80319214 1170.72277832 425.70022583 -146.80319214
		 456.61938477 425.70022583 -281.74255371 1170.72277832 425.70022583 -281.74255371
		 456.61938477 364.5562439 -281.74255371 1170.72277832 364.5562439 -281.74255371 813.67108154 761.029785156 -146.80319214
		 813.67108154 822.17382813 -146.80319214 813.67108154 822.17382813 -281.74255371 813.67108154 761.029785156 -281.74255371;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 1
		 3 5 1 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 0 10 11 1 11 8 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 17 -3 -7
		mu 0 4 3 2 4 5
		f 4 2 18 -4 -9
		mu 0 4 5 4 6 7
		f 4 3 19 -1 -11
		mu 0 4 7 6 8 9
		f 4 -12 -10 -8 -6
		mu 0 4 10 11 12 13
		f 4 10 4 6 8
		mu 0 4 14 0 3 15
		f 4 -17 12 5 -14
		mu 0 4 2 1 10 13
		f 4 -18 13 7 -15
		mu 0 4 4 2 13 16
		f 4 -19 14 9 -16
		mu 0 4 6 4 16 17
		f 4 -20 15 11 -13
		mu 0 4 8 6 17 18;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface9" -p "Roof8";
	rename -uid "8819C9E6-164E-A8C7-5BC7-5E97F0E57FCF";
createNode mesh -n "polySurfaceShape30" -p "polySurface9";
	rename -uid "1313086C-3141-F5E2-CD75-838491E63A44";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 7 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[5].gtagnm" -type "string" "rim";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 6 "e[0]" "e[1]" "e[3]" "e[4]" "e[5]" "e[6]";
	setAttr ".gtag[6].gtagnm" -type "string" "top";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 0;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 6 ".uvst[0].uvsp[0:5]" -type "float2" 0.5 1 0 1 0 0 0.5
		 0 1 0 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".vt[0:5]"  543.74920654 519.77752686 -281.74255371 813.67108154 761.029785156 -281.74255371
		 1083.59301758 519.77752686 -281.74255371 543.74920654 519.77752686 -707.65393066
		 813.67108154 761.029785156 -707.65393066 1083.59301758 519.77752686 -707.65393066;
	setAttr -s 7 ".ed[0:6]"  0 3 0 0 1 0 1 4 0 1 2 0 2 5 0 3 4 0 4 5 0;
	setAttr -s 2 -ch 8 ".fc[0:1]" -type "polyFaces" 
		f 4 -6 -1 1 2
		mu 0 4 0 1 2 3
		f 4 4 -7 -3 3
		mu 0 4 4 5 0 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface10" -p "Roof8";
	rename -uid "259B8EB8-F645-4E64-FCAE-FAAF91D7FF9A";
	setAttr ".rp" -type "double3" 813.67108154296875 593.36503601074219 -775.12359619140614 ;
	setAttr ".sp" -type "double3" 813.67108154296875 593.36503601074219 -775.12359619140614 ;
createNode mesh -n "polySurfaceShape31" -p "polySurface10";
	rename -uid "B5EFC88E-BE47-775F-38E2-A8939BCE2F24";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 7 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "rim";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[6].gtagnm" -type "string" "top";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.5 0 0.5
		 0.25 0.375 0.25 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75 0.5 1 0.375 1 0.625 0 0.875
		 0 0.875 0.25 0.625 0.25 0.125 0 0.125 0.25 0.625 0.5 0.625 0.75 0.625 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  456.61938477 364.5562439 -707.65393066 1170.72277832 364.5562439 -707.65393066
		 456.61938477 425.70022583 -707.65393066 1170.72277832 425.70022583 -707.65393066
		 456.61938477 425.70022583 -842.59326172 1170.72277832 425.70022583 -842.59326172
		 456.61938477 364.5562439 -842.59326172 1170.72277832 364.5562439 -842.59326172 813.67108154 761.029785156 -707.65393066
		 813.67108154 822.17382813 -707.65393066 813.67108154 822.17382813 -842.59326172 813.67108154 761.029785156 -842.59326172;
	setAttr -s 20 ".ed[0:19]"  0 8 0 2 9 0 4 10 0 6 11 0 0 2 0 1 3 0 2 4 1
		 3 5 1 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 3 0 10 5 0 11 7 0 8 9 1 9 10 0 10 11 1 11 8 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 16 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 17 -3 -7
		mu 0 4 3 2 4 5
		f 4 2 18 -4 -9
		mu 0 4 5 4 6 7
		f 4 3 19 -1 -11
		mu 0 4 7 6 8 9
		f 4 -12 -10 -8 -6
		mu 0 4 10 11 12 13
		f 4 10 4 6 8
		mu 0 4 14 0 3 15
		f 4 -17 12 5 -14
		mu 0 4 2 1 10 13
		f 4 -18 13 7 -15
		mu 0 4 4 2 13 16
		f 4 -19 14 9 -16
		mu 0 4 6 4 16 17
		f 4 -20 15 11 -13
		mu 0 4 8 6 17 18;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Wood6";
	rename -uid "F1C1C422-634D-7A2C-0EDC-C19649FCE70A";
	setAttr ".rp" -type "double3" 807.03801219992511 449.73868002597999 -675.86052543803692 ;
	setAttr ".sp" -type "double3" 807.03801219992511 449.73868002597999 -675.86052543803692 ;
createNode mesh -n "Wood6Shape" -p "Wood6";
	rename -uid "8F1EC3E2-3B47-EE2E-2051-3286837BC342";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  1017.4532 430.23868 -637.35815 
		1016.4532 470.23868 -637.35815 597.62286 429.23868 -637.35815 596.62286 469.23868 
		-637.35815 597.62286 429.23868 -714.36292 596.62286 469.23868 -714.36292 1017.4532 
		430.23868 -714.36292 1016.4532 470.23868 -714.36292;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder5";
	rename -uid "A2007C78-8744-FD21-0F6E-069B0F706A73";
	setAttr ".rp" -type "double3" 870.44655776958678 88.237232544155404 -256.39291454793715 ;
	setAttr ".sp" -type "double3" 870.44655776958678 88.237232544155404 -256.39291454793715 ;
createNode mesh -n "ladder5Shape" -p "ladder5";
	rename -uid "F4290653-A347-AA71-85BB-23B8DEA8ED12";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  878.23157 45.168259 -177.96555 
		877.23157 9.8098621 -213.32394 878.23157 166.6646 -300.46188 877.23157 131.3062 -335.82028 
		863.66156 166.6646 -299.46188 862.66156 131.3062 -334.82028 863.66156 45.168259 -176.96555 
		862.66156 9.8098621 -212.32394;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder6";
	rename -uid "89179B62-4E4A-B0E5-4A40-5AA9D5B40F7C";
	setAttr ".rp" -type "double3" 749.29982031703037 88.237232544155404 -265.28532226981781 ;
	setAttr ".sp" -type "double3" 749.29982031703037 88.237232544155404 -265.28532226981781 ;
createNode mesh -n "ladder6Shape" -p "ladder6";
	rename -uid "09AA84C7-D14B-0BCD-CD0A-C48C3AA2ACC8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  757.08478 45.168259 -186.85796 
		756.08478 9.8098621 -222.21635 757.08478 166.6646 -309.35431 756.08478 131.3062 -344.71271 
		742.51483 166.6646 -308.35431 741.51483 131.3062 -343.71271 742.51483 45.168259 -185.85796 
		741.51483 9.8098621 -221.21635;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ladder7";
	rename -uid "F34854A0-7041-D611-AB81-15B9D45ACFED";
	setAttr ".rp" -type "double3" 751.01520809250906 105.19126574686202 -310.74873411807704 ;
	setAttr ".sp" -type "double3" 751.01520809250906 105.19126574686202 -310.74873411807704 ;
createNode mesh -n "ladderShape7" -p "ladder7";
	rename -uid "3EBB25D5-7C4F-CEDA-BFA0-B0AD13ADAD7F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  751.5152 105.69127 -269.22284 
		866.64459 105.69127 -269.22284 751.5152 118.7387 -269.22284 866.64459 118.7387 -269.22284 
		751.5152 118.7387 -310.24875 866.64459 118.7387 -310.24875 751.5152 105.69127 -310.24875 
		866.64459 105.69127 -310.24875;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "lantern";
	rename -uid "9C063344-9540-D4FC-418B-53AA71B0C948";
	setAttr ".rp" -type "double3" 958.85352454027463 468.50128173828125 -244.26138305664062 ;
	setAttr ".sp" -type "double3" 958.85352454027463 468.50128173828125 -244.26138305664062 ;
createNode mesh -n "lanternShape" -p "lantern";
	rename -uid "2C606862-E146-1945-2472-F0A1C6E5F178";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 3 "f[1]" "f[3]" "f[6:9]";
	setAttr ".iog[0].og[1].gcl" -type "componentList" 3 "f[0]" "f[2]" "f[4:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[6:9]";
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 18 ".uvst[0].uvsp[0:17]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0.25 0.625 0.25 0.625 0.5 0.375 0.5;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 13 ".pt";
	setAttr ".pt[0]" -type "float3" 942.03137 384.64276 -230.88536 ;
	setAttr ".pt[1]" -type "float3" 966.47229 384.64276 -230.88536 ;
	setAttr ".pt[2]" -type "float3" 931.41943 433.13544 -223.2028 ;
	setAttr ".pt[3]" -type "float3" 979.91205 433.13544 -223.2028 ;
	setAttr ".pt[4]" -type "float3" 931.41943 433.13544 -271.69547 ;
	setAttr ".pt[5]" -type "float3" 979.91205 433.13544 -271.69547 ;
	setAttr ".pt[6]" -type "float3" 942.03137 384.64276 -262.05154 ;
	setAttr ".pt[7]" -type "float3" 966.47229 384.64276 -262.05154 ;
	setAttr ".pt[8]" -type "float3" 952.97803 468.00128 -244.76138 ;
	setAttr ".pt[9]" -type "float3" 958.35352 468.00128 -244.76138 ;
	setAttr ".pt[10]" -type "float3" 958.35352 468.00128 -250.13687 ;
	setAttr ".pt[11]" -type "float3" 952.97803 468.00128 -250.13687 ;
	setAttr ".pt[14]" -type "float3" 4.4703484e-08 -3.2782555e-07 1.7881393e-07 ;
	setAttr -s 12 ".vt[0:11]"  -0.50000048 -0.50000024 0.50000012 0.49999952 -0.50000024 0.50000012
		 -0.50000048 0.50000024 0.50000012 0.49999952 0.50000024 0.50000012 -0.50000048 0.50000024 -0.50000006
		 0.49999952 0.50000024 -0.50000006 -0.50000048 -0.50000024 -0.50000006 0.49999952 -0.50000024 -0.50000006
		 -0.50000048 0.50000024 0.50000012 0.49999952 0.50000024 0.50000012 0.49999952 0.50000024 -0.50000006
		 -0.50000048 0.50000024 -0.50000006;
	setAttr -s 20 ".ed[0:19]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 2 8 0 3 9 0 8 9 0 5 10 0 9 10 0 4 11 0 11 10 0 8 11 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 14 16 -19 -20
		mu 0 4 14 15 16 17
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 1 13 -15 -13
		mu 0 4 2 3 15 14
		f 4 7 15 -17 -14
		mu 0 4 3 5 16 15
		f 4 -3 17 18 -16
		mu 0 4 5 4 17 16
		f 4 -7 12 19 -18
		mu 0 4 4 2 14 17;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel1";
	rename -uid "8CE2057D-1946-EE55-A11B-2B88476C049B";
	setAttr ".t" -type "double3" 2095.5052280920459 524.21785849663274 153.46359435696274 ;
	setAttr ".r" -type "double3" 0 55 0 ;
	setAttr ".s" -type "double3" 120.86504012670733 100.32053255607232 132.21642203641434 ;
	setAttr ".rp" -type "double3" 2.1612353319125485e-05 2.2737367544323206e-13 -2.3642138606860806e-05 ;
	setAttr ".rpt" -type "double3" -100.3206607664399 100.32060450624142 -1.6660078888096928e-05 ;
	setAttr ".sp" -type "double3" 1.7881393432617188e-07 0 -1.7881393432617188e-07 ;
	setAttr ".spt" -type "double3" 2.1433539384799313e-05 2.2737367544323206e-13 -2.3463324672534635e-05 ;
createNode transform -n "transform5" -p "millwheel1";
	rename -uid "A01BF181-D549-CBAB-B80C-8A92E53992F2";
	setAttr ".v" no;
createNode mesh -n "millwheel1Shape" -p "transform5";
	rename -uid "0E33CB3C-AB42-66CF-0638-5A89FF01CE4A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:9]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:8]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:10]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:21]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[11:21]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[9:17]";
	setAttr ".pv" -type "double2" 0.40624998509883881 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.39999998 0.3125
		 0.41249999 0.31250617 0.42499995 0.3125 0.43749997 0.31250355 0.44999993 0.3125 0.46249992
		 0.31250501 0.4749999 0.3125 0.48749989 0.31250274 0.49999988 0.3125 0.51249987 0.31250572
		 0.52499986 0.3125 0.53749985 0.31250316 0.54999983 0.3125 0.56249982 0.31250328 0.57499981
		 0.3125 0.59999979 0.3125 0.61249971 0.31250301 0.62499976 0.3125 0.39999998 0.6875
		 0.41249999 0.68749684 0.42499995 0.6875 0.43749997 0.68749422 0.44999993 0.6875 0.46249992
		 0.68749768 0.4749999 0.6875 0.48749989 0.68749452 0.49999988 0.6875 0.51249987 0.68749702
		 0.52499986 0.6875 0.53749985 0.6874941 0.54999983 0.6875 0.56249982 0.6874941 0.57499981
		 0.6875 0.59999979 0.6875 0.61249971 0.68749368 0.62499976 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.95105714 -1 -0.30901718 0 -1 6.3897109e-18
		 0.5877856 -1 -0.80901748 0 -1 -1.000000476837 -0.58778548 -1 -0.8090173 -0.95105678 -1 -0.30901706
		 -0.95105678 -1 0.30901706 -0.58778536 -1 0.80901712 -2.9802322e-08 -1 1.000000119209
		 0.58778524 -1 0.80901706 0.95105654 -1 0.309017 0.95105714 1 -0.30901718 0 1 -6.3897109e-18
		 0.5877856 1 -0.80901748 0 1 -1.000000476837 -0.58778548 1 -0.8090173 -0.95105678 1 -0.30901706
		 -0.95105678 1 0.30901706 -0.58778536 1 0.80901712 -2.9802322e-08 1 1.000000119209
		 0.58778524 1 0.80901706 0.95105654 1 0.309017;
	setAttr -s 31 ".ed[0:30]"  0 1 0 1 2 0 1 3 0 5 1 0 6 1 0 7 1 0 8 1 0
		 9 1 0 10 1 0 11 12 0 12 13 0 12 14 0 16 12 0 17 12 0 18 12 0 19 12 0 20 12 0 21 12 0
		 0 11 0 1 12 0 2 13 0 3 14 0 4 15 0 5 16 0 6 17 0 7 18 0 8 19 0 9 20 0 10 21 0 1 4 0
		 15 12 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 -2 19 10 -21
		mu 0 4 0 1 19 18
		f 4 -3 19 11 -22
		mu 0 4 2 3 21 20
		f 4 -30 19 -31 -23
		mu 0 4 4 5 23 22
		f 4 3 19 -13 -24
		mu 0 4 6 7 25 24
		f 4 4 19 -14 -25
		mu 0 4 8 9 27 26
		f 4 5 19 -15 -26
		mu 0 4 10 11 29 28
		f 4 6 19 -16 -27
		mu 0 4 12 13 31 30
		f 4 -8 27 16 -20
		mu 0 4 13 14 32 31
		f 4 8 19 -18 -29
		mu 0 4 15 16 34 33
		f 4 -1 18 9 -20
		mu 0 4 16 17 35 34;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "MillWheel2";
	rename -uid "4289CC35-E946-498D-873B-618DA8CB9117";
	setAttr ".rp" -type "double3" 1115.4214803149946 150.16239672307 -502.58168014786492 ;
	setAttr ".sp" -type "double3" 1115.4214803149946 150.16239672307 -502.58168014786492 ;
createNode mesh -n "MillWheel2Shape" -p "MillWheel2";
	rename -uid "BACABAB5-2D4E-93A1-BE17-998873893093";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".pv" -type "double2" 0.58749991655349731 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[0:27]" -type "float3"  1015.553 216.70168 -592.20099 
		1016.053 257.20715 -536.08667 1016.671 257.20715 -467.17459 1017.171 216.70164 -411.78683 
		1017.362 151.16238 -391.07965 1017.171 85.623123 -412.96243 1016.671 45.117638 -469.07672 
		1016.053 45.117641 -537.98877 1015.553 79.697998 -593.37653 1015.362 151.16238 -614.08374 
		1213.6719 214.70168 -592.20099 1214.1719 255.20715 -536.08667 1214.7899 255.20715 
		-467.17459 1215.2899 214.70164 -411.78683 1215.481 149.16238 -391.07965 1215.2899 
		83.623123 -412.96243 1214.7899 43.117638 -469.07672 1214.1719 43.117641 -537.98877 
		1213.6719 77.697998 -593.37653 1213.481 149.16238 -614.08374 1016.362 151.16238 -502.5817 
		1214.481 149.16238 -502.5817 0 0 1.4901161e-08 0 0 1.4901161e-08 0 0 1.4901161e-08 
		0 0 1.4901161e-08 0 0 -3.5527137e-15 0 0 -3.5527137e-15;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millwheel2";
	rename -uid "D0C9A6A7-8F41-57DD-993E-12B953A4350C";
	setAttr ".t" -type "double3" 2001.1042137292634 362.70593479483853 157.85738531457855 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" -5.3489120549142163e-05 168.74078369140625 0 ;
	setAttr ".sp" -type "double3" -3.1698988678385831e-07 1.0000000255942929 8.4217074070887601e-17 ;
	setAttr ".spt" -type "double3" -5.3172130662358312e-05 167.74078366581196 -8.4217074070887662e-17 ;
createNode transform -n "transform4" -p "millwheel2";
	rename -uid "31591E9B-D04E-9795-B322-28B28AE00F68";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform4";
	rename -uid "8A592D52-DC4E-D528-7CAC-E7947C13A35C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "millwheel6";
	rename -uid "D8C3C62A-854D-19DA-3A09-B886E3125BFA";
	setAttr ".t" -type "double3" 1995.1846238016205 609.651417434118 153.46359252929685 ;
	setAttr ".s" -type "double3" 168.74077937260532 26.766597055388214 168.74077937260532 ;
	setAttr ".rp" -type "double3" -5.3489120549142163e-05 115.20759135494451 2.9063158755413989e-14 ;
	setAttr ".sp" -type "double3" -3.1698988678385831e-07 -0.9999999081173172 -3.3306690738754696e-16 ;
	setAttr ".spt" -type "double3" -5.3172130662358312e-05 116.20759126306183 2.9396225662801542e-14 ;
createNode transform -n "transform6" -p "millwheel6";
	rename -uid "C90F9C24-1049-04A5-5064-EBA4A4D39F5D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform6";
	rename -uid "CE3DF2EC-5648-579A-186D-6882A4609035";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:9]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[20]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:9]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:19]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[10:19]" "vtx[21]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[10:19]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:9]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[20:29]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[10:19]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.62640893 0.064408526
		 0.54828387 0.0076473951 0.45171607 0.00764741 0.37359107 0.064408556 0.34375 0.15625
		 0.37359107 0.24809146 0.4517161 0.3048526 0.54828393 0.3048526 0.62640893 0.24809146
		 0.65625 0.15625 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125
		 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995 0.3125 0.57499993 0.3125
		 0.5999999 0.3125 0.62499988 0.3125 0.375 0.6875 0.40000001 0.6875 0.42500001 0.6875
		 0.45000002 0.6875 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875
		 0.57499993 0.6875 0.5999999 0.6875 0.62499988 0.6875 0.62640893 0.75190854 0.54828387
		 0.6951474 0.45171607 0.6951474 0.37359107 0.75190854 0.34375 0.84375 0.37359107 0.93559146
		 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893 0.93559146 0.65625 0.84375 0.5
		 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".vt[0:21]"  0.80901706 -1 -0.58778542 0.30901694 -1 -0.95105672
		 -0.30901715 -1 -0.9510566 -0.80901718 -1 -0.58778524 -1.000000119209 -1 5.9604645e-08
		 -0.80901706 -1 0.58778536 -0.30901697 -1 0.9510566 0.30901703 -1 0.95105654 0.809017 -1 0.58778524
		 1 -1 0 0.80901706 1 -0.58778542 0.30901694 1 -0.95105672 -0.30901715 1 -0.9510566
		 -0.80901718 1 -0.58778524 -1.000000119209 1 5.9604645e-08 -0.80901706 1 0.58778536
		 -0.30901697 1 0.9510566 0.30901703 1 0.95105654 0.809017 1 0.58778524 1 1 0 0 -1 0
		 0 1 0;
	setAttr -s 50 ".ed[0:49]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 10 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0
		 20 0 1 20 1 1 20 2 1 20 3 1 20 4 1 20 5 1 20 6 1 20 7 1 20 8 1 20 9 1 10 21 1 11 21 1
		 12 21 1 13 21 1 14 21 1 15 21 1 16 21 1 17 21 1 18 21 1 19 21 1;
	setAttr -s 30 -ch 100 ".fc[0:29]" -type "polyFaces" 
		f 4 0 21 -11 -21
		mu 0 4 10 11 22 21
		f 4 1 22 -12 -22
		mu 0 4 11 12 23 22
		f 4 2 23 -13 -23
		mu 0 4 12 13 24 23
		f 4 3 24 -14 -24
		mu 0 4 13 14 25 24
		f 4 4 25 -15 -25
		mu 0 4 14 15 26 25
		f 4 5 26 -16 -26
		mu 0 4 15 16 27 26
		f 4 6 27 -17 -27
		mu 0 4 16 17 28 27
		f 4 7 28 -18 -28
		mu 0 4 17 18 29 28
		f 4 8 29 -19 -29
		mu 0 4 18 19 30 29
		f 4 9 20 -20 -30
		mu 0 4 19 20 31 30
		f 3 -1 -31 31
		mu 0 3 1 0 42
		f 3 -2 -32 32
		mu 0 3 2 1 42
		f 3 -3 -33 33
		mu 0 3 3 2 42
		f 3 -4 -34 34
		mu 0 3 4 3 42
		f 3 -5 -35 35
		mu 0 3 5 4 42
		f 3 -6 -36 36
		mu 0 3 6 5 42
		f 3 -7 -37 37
		mu 0 3 7 6 42
		f 3 -8 -38 38
		mu 0 3 8 7 42
		f 3 -9 -39 39
		mu 0 3 9 8 42
		f 3 -10 -40 30
		mu 0 3 0 9 42
		f 3 10 41 -41
		mu 0 3 40 39 43
		f 3 11 42 -42
		mu 0 3 39 38 43
		f 3 12 43 -43
		mu 0 3 38 37 43
		f 3 13 44 -44
		mu 0 3 37 36 43
		f 3 14 45 -45
		mu 0 3 36 35 43
		f 3 15 46 -46
		mu 0 3 35 34 43
		f 3 16 47 -47
		mu 0 3 34 33 43
		f 3 17 48 -48
		mu 0 3 33 32 43
		f 3 18 49 -49
		mu 0 3 32 41 43
		f 3 19 40 -50
		mu 0 3 41 40 43;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "millWheel";
	rename -uid "2B24517A-2C4F-9F58-84EE-FBB5D643FA66";
	setAttr ".rp" -type "double3" 1212.2899795906437 248.99906668573374 -489.18135139742958 ;
	setAttr ".sp" -type "double3" 1212.2899795906437 248.99906668573374 -489.18135139742958 ;
createNode mesh -n "millWheelShape" -p "millWheel";
	rename -uid "A2B664A5-AD4D-5400-6B6F-26B6B0F2ED3C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[10:19]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "e[0:9]" "e[20:28]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 3 "vtx[0:10]" "vtx[12:22]" "vtx[24]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[12:22]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "vtx[0:9]" "vtx[12:23]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 3 "vtx[11]" "vtx[23]" "vtx[25]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[23]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 3 "f[0:9]" "f[30:49]" "f[60:89]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 2 "f[20:29]" "f[50:59]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 2 "e[29:37]" "e[51:60]";
	setAttr ".pv" -type "double2" 0.49999743700027466 0.5000007557682693 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 126 ".uvst[0].uvsp[0:125]" -type "float2" 0.62641698 0.064409517
		 0.54828054 0.0076465225 0.45171613 0.0076474319 0.37359324 0.064409368 0.34374911
		 0.15624982 0.37359628 0.24809268 0.4517197 0.30485687 0.54829061 0.30485424 0.62640852
		 0.2480927 0.65624505 0.15625188 0.37499869 0.31249389 0.39999986 0.31249923 0.42499998
		 0.31249991 0.45000002 0.3125 0.47500002 0.3125 0.5 0.3125 0.52499998 0.3125 0.54999995
		 0.3125 0.57499993 0.3125 0.5999999 0.3125 0.62501574 0.31250769 0.5 0.15625 0.5 0.84375
		 0.39610851 0.31250879 0.41249999 0.31250617 0.41249999 0.68749684 0.39610851 0.68749958
		 0.42155018 0.31250474 0.43749997 0.31250355 0.43749997 0.68749422 0.42155018 0.68749553
		 0.44607958 0.31250525 0.46249992 0.31250501 0.46249992 0.68749768 0.44607958 0.68749803
		 0.47020566 0.31250334 0.48749989 0.31250274 0.48749989 0.68749452 0.47020566 0.68749523
		 0.49522278 0.31250566 0.51249987 0.31250572 0.51249987 0.68749702 0.49522278 0.68749708
		 0.52109641 0.31251115 0.53749985 0.31250316 0.53749985 0.6874941 0.52109641 0.68750221
		 0.54654968 0.31250453 0.56249982 0.31250328 0.56249982 0.6874941 0.54654968 0.68749547
		 0.57891899 0.31250668 0.57891899 0.68749762 0.5952093 0.3125048 0.61249971 0.31250301
		 0.61249971 0.68749368 0.5952093 0.68749559 0.62976772 0.31251696 0.62976772 0.68750775
		 0.40000057 0.68750089 0.37500453 0.68750715 0.42500007 0.68750012 0.45000002 0.6875
		 0.47500002 0.6875 0.5 0.6875 0.52499998 0.6875 0.54999995 0.6875 0.57499993 0.6875
		 0.5999999 0.6875 0.62499601 0.6875006 0.5 0.15625 0.62640494 0.93558884 0.54828525
		 0.99235499 0.5 0.84375 0.4517107 0.99234712 0.37358746 0.9355948 0.34375119 0.8437593
		 0.37358969 0.7519052 0.4517149 0.69514656 0.5482828 0.6951499 0.62641203 0.75190377
		 0.65624577 0.84375495 0.375 0.3125 0.40000001 0.3125 0.40000057 0.68750089 0.37500453
		 0.68750715 0.42500001 0.3125 0.42500007 0.68750012 0.45000002 0.3125 0.45000002 0.6875
		 0.47500002 0.3125 0.47500002 0.6875 0.5 0.3125 0.5 0.6875 0.52499998 0.3125 0.52499998
		 0.6875 0.54999995 0.3125 0.54999995 0.6875 0.57499993 0.3125 0.57499993 0.6875 0.5999999
		 0.3125 0.5999999 0.6875 0.62499988 0.3125 0.62499601 0.6875006 0.37499869 0.31249389
		 0.39999986 0.31249923 0.40000001 0.6875 0.375 0.6875 0.42499998 0.31249991 0.42500001
		 0.6875 0.45000002 0.3125 0.45000002 0.6875 0.47500002 0.3125 0.47500002 0.6875 0.5
		 0.3125 0.5 0.6875 0.52499998 0.3125 0.52499998 0.6875 0.54999995 0.3125 0.54999995
		 0.6875 0.57499993 0.3125 0.57499993 0.6875 0.5999999 0.3125 0.5999999 0.6875 0.62501574
		 0.31250769 0.62499988 0.6875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".pt[0:65]" -type "float3"  -417.16058 -592.26599 -1455.7579 
		-332.79022 -530.96729 -1310.0889 -228.50261 -530.96729 -1205.8013 -144.13225 -592.26599 
		-1182.7296 -111.90556 -691.44934 -1249.6863 -144.13225 -790.63269 -1381.0963 -228.50261 
		-851.9314 -1526.7654 -332.79022 -851.9314 -1631.053 -417.16058 -790.63269 -1654.1246 
		-449.38715 -691.44934 -1587.1678 -280.64642 -691.44934 -1418.4271 -227.1132 -744.98254 
		-1418.4271 -533.4314 -330.32611 -1310.0889 -481.2876 -490.80817 -1418.4271 -429.1438 
		-330.32611 -1205.8013 -344.77344 -391.62482 -1182.7296 -312.54675 -490.80817 -1249.6863 
		-344.77344 -589.99152 -1381.0963 -429.1438 -651.29022 -1526.7654 -533.4314 -651.29022 
		-1631.053 -617.80176 -589.99152 -1654.1246 -650.02832 -490.80817 -1587.1678 -617.80176 
		-391.62482 -1455.7579 -280.64642 -691.44934 -1418.4271 -534.8208 -437.27496 -1418.4271 
		-481.2876 -490.80817 -1418.4271 -714.82764 -304.54272 -1465.7018 -607.98376 -226.91606 
		-1281.2312 -540.19104 -294.7088 -1281.2312 -647.03491 -372.33545 -1465.7018 -475.91736 
		-226.91606 -1149.1648 -408.12463 -294.7088 -1149.1648 -369.07361 -304.54272 -1119.9478 
		-301.28088 -372.33545 -1119.9478 -328.2627 -430.1452 -1204.7393 -260.46997 -497.93793 
		-1204.7393 -369.07361 -555.74774 -1371.1527 -301.28088 -623.54047 -1371.1527 -475.91736 
		-633.37433 -1555.623 -408.12463 -701.16705 -1555.623 -607.98376 -633.37433 -1687.6895 
		-540.19104 -701.16705 -1687.6895 -714.82764 -555.74774 -1716.9067 -647.03491 -623.54047 
		-1716.9067 -755.63831 -430.1452 -1632.1149 -687.84558 -497.93793 -1632.1149 -458.84369 
		-560.11285 -1465.288 -352.93488 -483.16553 -1282.4318 -285.73541 -550.36499 -1282.4318 
		-391.64423 -627.31232 -1465.288 -222.02422 -483.16553 -1151.5211 -154.82475 -550.36499 
		-1151.5211 -116.1154 -560.11285 -1122.5597 -48.915939 -627.31232 -1122.5597 -75.661789 
		-684.61621 -1206.6094 -8.462327 -751.81567 -1206.6094 -116.1154 -809.11951 -1371.5663 
		-48.915939 -876.31897 -1371.5663 -222.02422 -886.06689 -1554.4225 -154.82475 -953.26636 
		-1554.4225 -352.93488 -886.06689 -1685.3331 -285.73541 -953.26636 -1685.3331 -458.84369 
		-809.11951 -1714.2946 -391.64423 -876.31897 -1714.2946 -499.29718 -684.61621 -1630.2448 
		-432.09772 -751.81567 -1630.2448;
	setAttr -s 66 ".vt[0:65]"  1883.625 940.44836426 830.06237793 1799.25463867 940.44836426 768.76367188
		 1694.96704102 940.44836426 768.76367188 1610.59667969 940.44836426 830.06237793 1578.36999512 940.44836426 929.24572754
		 1610.59667969 940.44836426 1028.42907715 1694.96704102 940.44836426 1089.7277832
		 1799.25463867 940.44836426 1089.7277832 1883.625 940.44836426 1028.42907715 1915.8515625 940.44836426 929.24572754
		 1747.11083984 940.44836426 929.24572754 1747.11083984 993.98156738 929.24572754 1799.25463867 739.80718994 768.76367188
		 1747.11083984 739.80718994 929.24572754 1694.96704102 739.80718994 768.76367188 1610.59667969 739.80718994 830.06237793
		 1578.36999512 739.80718994 929.24572754 1610.59667969 739.80718994 1028.42907715
		 1694.96704102 739.80718994 1089.7277832 1799.25463867 739.80718994 1089.7277832 1883.625 739.80718994 1028.42907715
		 1915.8515625 739.80718994 929.24572754 1883.625 739.80718994 830.06237793 1747.11083984 940.44836426 929.24572754
		 1747.11083984 686.27398682 929.24572754 1747.11083984 739.80718994 929.24572754 1919.98791504 679.14422607 803.64324951
		 1813.14404297 679.14422607 726.016601563 1813.14404297 746.93695068 726.016601563
		 1919.98791504 746.93695068 803.64324951 1681.077636719 679.14422607 726.016601563
		 1681.077636719 746.93695068 726.016601563 1574.23388672 679.14422607 803.64324951
		 1574.23388672 746.93695068 803.64324951 1533.42297363 679.14422607 929.24572754 1533.42297363 746.93695068 929.24572754
		 1574.23388672 679.14422607 1054.8482666 1574.23388672 746.93695068 1054.8482666 1681.077636719 679.14422607 1132.47485352
		 1681.077636719 746.93695068 1132.47485352 1813.14404297 679.14422607 1132.47485352
		 1813.14404297 746.93695068 1132.47485352 1919.98791504 679.14422607 1054.8482666
		 1919.98791504 746.93695068 1054.8482666 1960.79858398 679.14422607 929.24572754 1960.79858398 746.93695068 929.24572754
		 1918.47497559 933.61523438 804.74237061 1812.56616211 933.61523438 727.79504395 1812.56616211 1000.81469727 727.79504395
		 1918.47497559 1000.81469727 804.74237061 1681.65551758 933.61523438 727.79504395
		 1681.65551758 1000.81469727 727.79504395 1575.7467041 933.61523438 804.74237061 1575.7467041 1000.81469727 804.74237061
		 1535.29309082 933.61523438 929.24572754 1535.29309082 1000.81469727 929.24572754
		 1575.7467041 933.61523438 1053.74902344 1575.7467041 1000.81469727 1053.74902344
		 1681.65551758 933.61523438 1130.69641113 1681.65551758 1000.81469727 1130.69641113
		 1812.56616211 933.61523438 1130.69641113 1812.56616211 1000.81469727 1130.69641113
		 1918.47497559 933.61523438 1053.74902344 1918.47497559 1000.81469727 1053.74902344
		 1958.9284668 933.61523438 929.24572754 1958.9284668 1000.81469727 929.24572754;
	setAttr -s 171 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 8 1 8 9 1
		 9 0 1 10 0 1 10 1 1 10 2 1 10 3 1 10 4 1 10 5 1 10 6 1 10 7 1 10 8 1 10 9 1 12 13 0
		 13 14 0 13 15 0 17 13 0 18 13 0 19 13 0 20 13 0 21 13 0 22 13 0 1 23 0 23 2 0 23 3 0
		 5 23 0 6 23 0 7 23 0 8 23 0 9 23 0 0 23 0 12 1 0 13 23 0 14 2 0 15 3 0 16 4 0 17 5 0
		 18 6 0 19 7 0 20 8 0 21 9 0 22 0 0 13 16 0 4 23 0 22 12 1 12 14 1 14 15 1 15 16 1
		 16 17 1 17 18 1 18 19 1 19 20 1 20 21 1 21 22 1 22 25 1 12 25 1 14 25 1 15 25 1 16 25 1
		 17 25 1 18 25 1 19 25 1 20 25 1 21 25 1 24 26 1 24 27 1 26 27 0 12 28 1 27 28 1 22 29 1
		 29 28 0 26 29 1 24 30 1 27 30 0 14 31 1 30 31 1 28 31 0 24 32 1 30 32 0 15 33 1 32 33 1
		 31 33 0 24 34 1 32 34 0 16 35 1 34 35 1 33 35 0 24 36 1 34 36 0 17 37 1 36 37 1 35 37 0
		 24 38 1 36 38 0 18 39 1 38 39 1 37 39 0 24 40 1 38 40 0 19 41 1 40 41 1 39 41 0 24 42 1
		 40 42 0 20 43 1 42 43 1 41 43 0 24 44 1 42 44 0 21 45 1 44 45 1 43 45 0 44 26 0 45 29 0
		 0 46 1 1 47 1 46 47 0 11 48 1 47 48 1 11 49 1 49 48 0 46 49 1 2 50 1 47 50 0 11 51 1
		 50 51 1 48 51 0 3 52 1 50 52 0 11 53 1 52 53 1 51 53 0 4 54 1 52 54 0 11 55 1 54 55 1
		 53 55 0 5 56 1 54 56 0 11 57 1 56 57 1 55 57 0 6 58 1 56 58 0 11 59 1 58 59 1 57 59 0
		 7 60 1 58 60 0 11 61 1 60 61 1 59 61 0 8 62 1 60 62 0 11 63 1 62 63 1 61 63 0 9 64 1
		 62 64 0;
	setAttr ".ed[166:170]" 11 65 1 64 65 1 63 65 0 64 46 0 65 49 0;
	setAttr -s 90 -ch 320 ".fc[0:89]" -type "polyFaces" 
		f 4 123 125 -128 -129
		mu 0 4 104 105 106 107
		f 4 130 132 -134 -126
		mu 0 4 105 108 109 106
		f 4 135 137 -139 -133
		mu 0 4 108 110 111 109
		f 4 140 142 -144 -138
		mu 0 4 110 112 113 111
		f 4 145 147 -149 -143
		mu 0 4 112 114 115 113
		f 4 150 152 -154 -148
		mu 0 4 114 116 117 115
		f 4 155 157 -159 -153
		mu 0 4 116 118 119 117
		f 4 160 162 -164 -158
		mu 0 4 118 120 121 119
		f 4 165 167 -169 -163
		mu 0 4 120 122 123 121
		f 4 169 128 -171 -168
		mu 0 4 122 124 125 123
		f 3 -1 -11 11
		mu 0 3 1 0 21
		f 3 -2 -12 12
		mu 0 3 2 1 21
		f 3 -3 -13 13
		mu 0 3 3 2 21
		f 3 -4 -14 14
		mu 0 3 4 3 21
		f 3 -5 -15 15
		mu 0 3 5 4 21
		f 3 -6 -16 16
		mu 0 3 6 5 21
		f 3 -7 -17 17
		mu 0 3 7 6 21
		f 3 -8 -18 18
		mu 0 3 8 7 21
		f 3 -9 -19 19
		mu 0 3 9 8 21
		f 3 -10 -20 10
		mu 0 3 0 9 21
		f 3 126 127 -125
		mu 0 3 22 107 106
		f 3 124 133 -132
		mu 0 3 22 106 109
		f 3 131 138 -137
		mu 0 3 22 109 111
		f 3 136 143 -142
		mu 0 3 22 111 113
		f 3 141 148 -147
		mu 0 3 22 113 115
		f 3 146 153 -152
		mu 0 3 22 115 117
		f 3 151 158 -157
		mu 0 3 22 117 119
		f 3 156 163 -162
		mu 0 3 22 119 121
		f 3 161 168 -167
		mu 0 3 22 121 123
		f 3 166 170 -127
		mu 0 3 22 123 125
		f 4 -22 39 30 -41
		mu 0 4 23 24 25 26
		f 4 -23 39 31 -42
		mu 0 4 27 28 29 30
		f 4 -50 39 -51 -43
		mu 0 4 31 32 33 34
		f 4 23 39 -33 -44
		mu 0 4 35 36 37 38
		f 4 24 39 -34 -45
		mu 0 4 39 40 41 42
		f 4 25 39 -35 -46
		mu 0 4 43 44 45 46
		f 4 26 39 -36 -47
		mu 0 4 47 48 49 50
		f 4 -28 47 36 -40
		mu 0 4 48 51 52 49
		f 4 28 39 -38 -49
		mu 0 4 53 54 55 56
		f 4 -21 38 29 -40
		mu 0 4 54 57 58 55
		f 4 73 75 -78 -79
		mu 0 4 82 83 84 85
		f 4 80 82 -84 -76
		mu 0 4 83 86 87 84
		f 4 85 87 -89 -83
		mu 0 4 86 88 89 87
		f 4 90 92 -94 -88
		mu 0 4 88 90 91 89
		f 4 95 97 -99 -93
		mu 0 4 90 92 93 91
		f 4 100 102 -104 -98
		mu 0 4 92 94 95 93
		f 4 105 107 -109 -103
		mu 0 4 94 96 97 95
		f 4 110 112 -114 -108
		mu 0 4 96 98 99 97
		f 4 115 117 -119 -113
		mu 0 4 98 100 101 99
		f 4 119 78 -121 -118
		mu 0 4 100 102 103 101
		f 3 51 62 -62
		mu 0 3 71 72 73
		f 3 52 63 -63
		mu 0 3 72 74 73
		f 3 53 64 -64
		mu 0 3 74 75 73
		f 3 54 65 -65
		mu 0 3 75 76 73
		f 3 55 66 -66
		mu 0 3 76 77 73
		f 3 56 67 -67
		mu 0 3 77 78 73
		f 3 57 68 -68
		mu 0 3 78 79 73
		f 3 58 69 -69
		mu 0 3 79 80 73
		f 3 59 70 -70
		mu 0 3 80 81 73
		f 3 60 61 -71
		mu 0 3 81 71 73
		f 3 72 -74 -72
		mu 0 3 70 83 82
		f 4 -52 76 77 -75
		mu 0 4 59 60 85 84
		f 3 79 -81 -73
		mu 0 3 70 86 83
		f 4 -53 74 83 -82
		mu 0 4 61 59 84 87
		f 3 84 -86 -80
		mu 0 3 70 88 86
		f 4 -54 81 88 -87
		mu 0 4 62 61 87 89
		f 3 89 -91 -85
		mu 0 3 70 90 88
		f 4 -55 86 93 -92
		mu 0 4 63 62 89 91
		f 3 94 -96 -90
		mu 0 3 70 92 90
		f 4 -56 91 98 -97
		mu 0 4 64 63 91 93
		f 3 99 -101 -95
		mu 0 3 70 94 92
		f 4 -57 96 103 -102
		mu 0 4 65 64 93 95
		f 3 104 -106 -100
		mu 0 3 70 96 94
		f 4 -58 101 108 -107
		mu 0 4 66 65 95 97
		f 3 109 -111 -105
		mu 0 3 70 98 96
		f 4 -59 106 113 -112
		mu 0 4 67 66 97 99
		f 3 114 -116 -110
		mu 0 3 70 100 98
		f 4 -60 111 118 -117
		mu 0 4 68 67 99 101
		f 3 71 -120 -115
		mu 0 3 70 102 100
		f 4 -61 116 120 -77
		mu 0 4 69 68 101 103
		f 4 0 122 -124 -122
		mu 0 4 10 11 105 104
		f 4 1 129 -131 -123
		mu 0 4 11 12 108 105
		f 4 2 134 -136 -130
		mu 0 4 12 13 110 108
		f 4 3 139 -141 -135
		mu 0 4 13 14 112 110
		f 4 4 144 -146 -140
		mu 0 4 14 15 114 112
		f 4 5 149 -151 -145
		mu 0 4 15 16 116 114
		f 4 6 154 -156 -150
		mu 0 4 16 17 118 116
		f 4 7 159 -161 -155
		mu 0 4 17 18 120 118
		f 4 8 164 -166 -160
		mu 0 4 18 19 122 120
		f 4 9 121 -170 -165
		mu 0 4 19 20 124 122;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Trunk1";
	rename -uid "FF2FDE29-6D48-982B-6831-0198DAA13029";
	setAttr ".t" -type "double3" 583.32574256727094 -17.490011846455502 0 ;
	setAttr ".r" -type "double3" 0 33.254399165795959 0 ;
	setAttr ".rp" -type "double3" 676.09765625 142.17582698793689 553.68967466639515 ;
	setAttr ".sp" -type "double3" 676.09765625 142.17582698793689 553.68967466639515 ;
createNode transform -n "polySurface1" -p "Trunk1";
	rename -uid "06544242-704D-9D9A-D6ED-43AEE8F33399";
	setAttr ".rp" -type "double3" -1.4210854715202004e-14 0 0 ;
	setAttr ".sp" -type "double3" -1.4210854715202004e-14 0 0 ;
createNode mesh -n "polySurfaceShape10" -p "polySurface1";
	rename -uid "DDF9D391-9D4B-7F8A-C5BF-8D9CA8B16541";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 14 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 8 "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]";
	setAttr ".gtag[2].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 8 "e[0]" "e[1]" "e[2]" "e[3]" "e[4]" "e[5]" "e[6]" "e[7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 9 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]" "vtx[16]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 8 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 16 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]" "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 9 "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]" "vtx[17]";
	setAttr ".gtag[7].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 8 "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]";
	setAttr ".gtag[8].gtagnm" -type "string" "front";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "left";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[10].gtagnm" -type "string" "right";
	setAttr ".gtag[10].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[11].gtagnm" -type "string" "sides";
	setAttr ".gtag[11].gtagcmp" -type "componentList" 8 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]";
	setAttr ".gtag[12].gtagnm" -type "string" "top";
	setAttr ".gtag[12].gtagcmp" -type "componentList" 8 "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]";
	setAttr ".gtag[13].gtagnm" -type "string" "topRing";
	setAttr ".gtag[13].gtagcmp" -type "componentList" 8 "e[8]" "e[9]" "e[10]" "e[11]" "e[12]" "e[13]" "e[14]" "e[15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.40625 0.6875 0.375 0.6875 0.4375 0.3125 0.4375 0.6875 0.46875 0.3125 0.46875
		 0.6875 0.5 0.3125 0.5 0.6875 0.53125 0.3125 0.53125 0.6875 0.5625 0.3125 0.5625 0.6875
		 0.59375 0.3125 0.59375 0.6875 0.625 0.3125 0.625 0.6875 0.5 1.4901161e-08 0.61048543
		 0.04576458 0.5 0.15625 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543
		 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.61048543 0.95423543 0.5 1 0.5
		 0.84375 0.38951457 0.95423543 0.34375 0.84375 0.38951457 0.73326457 0.5 0.6875 0.61048543
		 0.73326457 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  702.23425293 215.71499634 334.0045166016
		 681.69476318 224.2227478 334.0045166016 661.15527344 215.71499634 334.0045166016
		 652.64752197 195.17550659 334.0045166016 661.15527344 174.63601685 334.0045166016
		 681.69476318 166.12826538 334.0045166016 702.23425293 174.63601685 334.0045166016
		 710.74200439 195.17550659 334.0045166016 702.23425293 215.71499634 773.37487793 681.69476318 224.2227478 773.37487793
		 661.15527344 215.71499634 773.37487793 652.64752197 195.17550659 773.37487793 661.15527344 174.63601685 773.37487793
		 681.69476318 166.12826538 773.37487793 702.23425293 174.63601685 773.37487793 710.74200439 195.17550659 773.37487793
		 681.69476318 195.17550659 334.0045166016 681.69476318 195.17550659 773.37487793;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 0 1 2 3
		f 4 1 18 -10 -18
		mu 0 4 1 4 5 2
		f 4 2 19 -11 -19
		mu 0 4 4 6 7 5
		f 4 3 20 -12 -20
		mu 0 4 6 8 9 7
		f 4 4 21 -13 -21
		mu 0 4 8 10 11 9
		f 4 5 22 -14 -22
		mu 0 4 10 12 13 11
		f 4 6 23 -15 -23
		mu 0 4 12 14 15 13
		f 4 7 16 -16 -24
		mu 0 4 14 16 17 15
		f 3 -1 -25 25
		mu 0 3 18 19 20
		f 3 -2 -26 26
		mu 0 3 21 18 20
		f 3 -3 -27 27
		mu 0 3 22 21 20
		f 3 -4 -28 28
		mu 0 3 23 22 20
		f 3 -5 -29 29
		mu 0 3 24 23 20
		f 3 -6 -30 30
		mu 0 3 25 24 20
		f 3 -7 -31 31
		mu 0 3 26 25 20
		f 3 -8 -32 24
		mu 0 3 19 26 20
		f 3 8 33 -33
		mu 0 3 27 28 29
		f 3 9 34 -34
		mu 0 3 28 30 29
		f 3 10 35 -35
		mu 0 3 30 31 29
		f 3 11 36 -36
		mu 0 3 31 32 29
		f 3 12 37 -37
		mu 0 3 32 33 29
		f 3 13 38 -38
		mu 0 3 33 34 29
		f 3 14 39 -39
		mu 0 3 34 35 29
		f 3 15 32 -40
		mu 0 3 35 27 29;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopBig2";
	rename -uid "13257305-914D-83A3-236F-2FA85BC62D90";
	setAttr ".rp" -type "double3" -867.2022558346398 934.36137398170706 -1105.4883910337539 ;
	setAttr ".sp" -type "double3" -867.2022558346398 934.36137398170706 -1105.4883910337539 ;
createNode mesh -n "TreetopBig2Shape" -p "TreetopBig2";
	rename -uid "755F8CF6-1748-A8AE-C5D0-6DBFA65613FA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt[0:33]" -type "float3"  -805.26147 813.7937 -1167.4292 
		-867.20227 813.7937 -1193.0859 -929.14307 813.7937 -1167.4292 -954.7998 813.7937 
		-1105.4884 -929.14307 813.7937 -1043.5476 -867.20227 813.7937 -1017.8908 -805.26147 
		813.7937 -1043.5476 -779.60474 813.7937 -1105.4884 -766.97992 888.30865 -1205.7107 
		-867.20227 888.30865 -1247.2241 -967.42456 888.30865 -1205.7107 -1008.938 888.30865 
		-1105.4884 -967.42456 888.30865 -1005.2661 -867.20227 888.30865 -963.75256 -766.97992 
		888.30865 -1005.2661 -725.46643 888.30865 -1105.4884 -766.97992 980.41412 -1205.7107 
		-867.20227 980.41412 -1247.2241 -967.42456 980.41412 -1205.7107 -1008.938 980.41412 
		-1105.4884 -967.42456 980.41412 -1005.2661 -867.20227 980.41412 -963.75256 -766.97992 
		980.41412 -1005.2661 -725.46643 980.41412 -1105.4884 -805.26147 1054.9291 -1167.4292 
		-867.20227 1054.9291 -1193.0859 -929.14307 1054.9291 -1167.4292 -954.7998 1054.9291 
		-1105.4884 -929.14307 1054.9291 -1043.5476 -867.20227 1054.9291 -1017.8908 -805.26147 
		1054.9291 -1043.5476 -779.60474 1054.9291 -1105.4884 -867.20227 785.33154 -1105.4884 
		-867.20227 1083.3912 -1105.4884;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1
		 7 0 1 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1
		 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1
		 29 30 1 30 31 1 31 24 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1
		 9 17 1 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 16 24 1 17 25 1 18 26 1 19 27 1
		 20 28 1 21 29 1 22 30 1 23 31 1 32 0 1 32 1 1 32 2 1 32 3 1 32 4 1 32 5 1 32 6 1
		 32 7 1 24 33 1 25 33 1 26 33 1 27 33 1 28 33 1 29 33 1 30 33 1 31 33 1;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopBig1";
	rename -uid "9183B8E4-8C42-E77E-70A3-C78A086D4D5D";
	setAttr ".rp" -type "double3" -578.59715287906863 1399.6144078986999 -1160.529435296869 ;
	setAttr ".sp" -type "double3" -578.59715287906863 1399.6144078986999 -1160.529435296869 ;
createNode mesh -n "TreetopBig1Shape" -p "TreetopBig1";
	rename -uid "F42377FD-794C-9EC7-B994-14BBD8F2C6C4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt[0:33]" -type "float3"  -414.33047 1079.8696 -1324.7961 
		-578.59717 1079.8696 -1392.8376 -742.86383 1079.8696 -1324.7961 -810.90533 1079.8696 
		-1160.5294 -742.86383 1079.8696 -996.2627 -578.59717 1079.8696 -928.22113 -414.33047 
		1079.8696 -996.2627 -346.28897 1079.8696 -1160.5294 -312.80807 1277.4828 -1426.3184 
		-578.59717 1277.4828 -1536.412 -844.38623 1277.4828 -1426.3184 -954.47968 1277.4828 
		-1160.5294 -844.38623 1277.4828 -894.7403 -578.59717 1277.4828 -784.64679 -312.80804 
		1277.4828 -894.7403 -202.71458 1277.4828 -1160.5294 -312.80807 1521.7461 -1426.3184 
		-578.59717 1521.7461 -1536.412 -844.38623 1521.7461 -1426.3184 -954.47968 1521.7461 
		-1160.5294 -844.38623 1521.7461 -894.7403 -578.59717 1521.7461 -784.64679 -312.80804 
		1521.7461 -894.7403 -202.71458 1521.7461 -1160.5294 -414.33047 1719.3593 -1324.7961 
		-578.59717 1719.3593 -1392.8376 -742.86383 1719.3593 -1324.7961 -810.90533 1719.3593 
		-1160.5294 -742.86383 1719.3593 -996.2627 -578.59717 1719.3593 -928.22113 -414.33047 
		1719.3593 -996.2627 -346.28897 1719.3593 -1160.5294 -578.59717 1004.3881 -1160.5294 
		-578.59717 1794.8407 -1160.5294;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1
		 7 0 1 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1
		 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1
		 29 30 1 30 31 1 31 24 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1
		 9 17 1 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 16 24 1 17 25 1 18 26 1 19 27 1
		 20 28 1 21 29 1 22 30 1 23 31 1 32 0 1 32 1 1 32 2 1 32 3 1 32 4 1 32 5 1 32 6 1
		 32 7 1 24 33 1 25 33 1 26 33 1 27 33 1 28 33 1 29 33 1 30 33 1 31 33 1;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchSmallTree";
	rename -uid "C3044183-B048-C273-3661-CD9717BB582E";
	setAttr ".rp" -type "double3" 237.0389104069161 378.81930510903157 -868.0801349434297 ;
	setAttr ".sp" -type "double3" 237.0389104069161 378.81930510903157 -868.0801349434297 ;
createNode mesh -n "BranchSmallTreeShape" -p "BranchSmallTree";
	rename -uid "FDEE458E-6242-6A5F-A7A5-81BAA593B07C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  154.21848 257.60696 -860.70898 
		127.25778 282.47046 -866.65497 111.32915 309.88794 -846.80444 115.76337 323.79861 
		-812.78552 137.96292 316.0538 -784.52606 164.92361 291.19031 -778.58002 180.85223 
		263.77286 -798.43054 176.41803 249.86217 -832.44952 331.16748 459.37329 -928.44769 
		320.61792 469.1022 -930.77435 314.38516 479.83047 -923.00696 316.12024 485.27365 
		-909.69556 324.80676 482.24316 -898.63782 335.35632 472.51422 -896.31116 341.58908 
		461.78595 -904.07855 339.854 456.3428 -917.38995 146.0907 286.83038 -822.61749 327.98712 
		470.80823 -913.54279;
	setAttr -s 18 ".vt[0:17]"  1.8071003 -1 -1.8071003 -9.2722544e-08 -1 -2.55562592
		 -1.8071003 -1 -1.8071003 -2.55562592 -1 -4.6361301e-08 -1.8071003 -1 1.8071003 -9.2722544e-08 -1 2.55562615
		 1.8071003 -1 1.8071003 2.55562615 -1 -4.6361301e-08 0.70710671 1 -0.70710671 0 1 -0.99999988
		 -0.70710671 1 -0.70710671 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994
		 0.70710677 1 0.70710677 1 1 0 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchBigTree";
	rename -uid "F6166895-D24C-FBCB-F05A-40B6F3D1AE73";
	setAttr ".rp" -type "double3" -699.14461121557235 794.1366598181304 -1136.7344129333424 ;
	setAttr ".sp" -type "double3" -699.14461121557235 794.1366598181304 -1136.7344129333424 ;
createNode mesh -n "BranchBigTreeShape" -p "BranchBigTree";
	rename -uid "A91782E1-4E4F-DB08-AC7B-B980708EBAF5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  -611.86902 688.69995 -1139.796 
		-590.84589 708.94861 -1122.7241 -572.02307 735.39429 -1131.0098 -566.42676 752.54547 
		-1159.7992 -577.33514 750.35522 -1192.2281 -598.35828 730.10657 -1209.3 -617.18109 
		703.66089 -1201.0144 -622.7774 686.5097 -1172.2249 -810.4436 856.68311 -1097.1986 
		-802.21735 864.60626 -1090.5184 -794.85211 874.95428 -1093.7605 -792.66229 881.66541 
		-1105.0258 -796.93066 880.80841 -1117.715 -805.15692 872.88525 -1124.395 -812.52216 
		862.53723 -1121.153 -814.71198 855.82605 -1109.8878 -594.60211 719.52759 -1166.0121 
		-803.68713 868.74573 -1107.4568;
	setAttr -s 18 ".vt[0:17]"  1.8071003 -1 -1.8071003 -9.2722544e-08 -1 -2.55562592
		 -1.8071003 -1 -1.8071003 -2.55562592 -1 -4.6361301e-08 -1.8071003 -1 1.8071003 -9.2722544e-08 -1 2.55562615
		 1.8071003 -1 1.8071003 2.55562615 -1 -4.6361301e-08 0.70710671 1 -0.70710671 0 1 -0.99999988
		 -0.70710671 1 -0.70710671 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994
		 0.70710677 1 0.70710677 1 1 0 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall1";
	rename -uid "5B520012-F84B-4774-EB6B-8182DCC5EBFD";
	setAttr ".rp" -type "double3" 143.02510776088042 980.31497425171665 -823.28832922587344 ;
	setAttr ".sp" -type "double3" 143.02510776088042 980.31497425171665 -823.28832922587344 ;
createNode mesh -n "TreetopSmall1Shape" -p "TreetopSmall1";
	rename -uid "FA488CD6-E442-FE2A-8055-2F86876E5BB7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt[0:33]" -type "float3"  266.84851 739.29297 -947.11176 
		143.02512 739.29297 -998.40106 19.201714 739.29297 -947.11176 -32.08762 739.29297 
		-823.28833 19.201714 739.29297 -699.4649 143.02512 739.29297 -648.17554 266.84851 
		739.29297 -699.4649 318.13785 739.29297 -823.28833 343.37558 888.25275 -1023.6387 
		143.02512 888.25275 -1106.6267 -57.325363 888.25275 -1023.6387 -140.31323 888.25275 
		-823.28833 -57.325363 888.25275 -622.93781 143.02512 888.25275 -539.94989 343.37561 
		888.25275 -622.93781 426.36349 888.25275 -823.28833 343.37558 1072.3772 -1023.6387 
		143.02512 1072.3772 -1106.6267 -57.325363 1072.3772 -1023.6387 -140.31323 1072.3772 
		-823.28833 -57.325363 1072.3772 -622.93781 143.02512 1072.3772 -539.94989 343.37561 
		1072.3772 -622.93781 426.36349 1072.3772 -823.28833 266.84851 1221.337 -947.11176 
		143.02512 1221.337 -998.40106 19.201714 1221.337 -947.11176 -32.08762 1221.337 -823.28833 
		19.201714 1221.337 -699.4649 143.02512 1221.337 -648.17554 266.84851 1221.337 -699.4649 
		318.13785 1221.337 -823.28833 143.02512 682.39532 -823.28833 143.02512 1278.2346 
		-823.28833;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1
		 7 0 1 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1
		 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1
		 29 30 1 30 31 1 31 24 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1
		 9 17 1 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 16 24 1 17 25 1 18 26 1 19 27 1
		 20 28 1 21 29 1 22 30 1 23 31 1 32 0 1 32 1 1 32 2 1 32 3 1 32 4 1 32 5 1 32 6 1
		 32 7 1 24 33 1 25 33 1 26 33 1 27 33 1 28 33 1 29 33 1 30 33 1 31 33 1;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall2";
	rename -uid "7136C0E9-B14E-D745-5CFB-AFB17C357528";
	setAttr ".rp" -type "double3" 346.02559682869492 602.76606440152 -928.95699943781733 ;
	setAttr ".sp" -type "double3" 346.02559682869492 602.76606440152 -928.95699943781733 ;
createNode mesh -n "TreetopSmall2Shape" -p "TreetopSmall2";
	rename -uid "81DBA63A-F14F-A23E-3384-3D9F4DBFD500";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt[0:33]" -type "float3"  407.9664 482.19839 -990.89783 
		346.0256 482.19839 -1016.5545 284.08481 482.19839 -990.89783 258.42807 482.19839 
		-928.95697 284.08481 482.19839 -867.01617 346.0256 482.19839 -841.35944 407.9664 
		482.19839 -867.01617 433.62314 482.19839 -928.95697 446.24792 556.71332 -1029.1793 
		346.0256 556.71332 -1070.6927 245.80327 556.71332 -1029.1793 204.28983 556.71332 
		-928.95697 245.80327 556.71332 -828.73462 346.0256 556.71332 -787.22119 446.24796 
		556.71332 -828.73462 487.76138 556.71332 -928.95697 446.24792 648.81879 -1029.1793 
		346.0256 648.81879 -1070.6927 245.80327 648.81879 -1029.1793 204.28983 648.81879 
		-928.95697 245.80327 648.81879 -828.73462 346.0256 648.81879 -787.22119 446.24796 
		648.81879 -828.73462 487.76138 648.81879 -928.95697 407.9664 723.33374 -990.89783 
		346.0256 723.33374 -1016.5545 284.08481 723.33374 -990.89783 258.42807 723.33374 
		-928.95697 284.08481 723.33374 -867.01617 346.0256 723.33374 -841.35944 407.9664 
		723.33374 -867.01617 433.62314 723.33374 -928.95697 346.0256 453.73624 -928.95697 
		346.0256 751.7959 -928.95697;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1
		 7 0 1 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1
		 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1
		 29 30 1 30 31 1 31 24 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1
		 9 17 1 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 16 24 1 17 25 1 18 26 1 19 27 1
		 20 28 1 21 29 1 22 30 1 23 31 1 32 0 1 32 1 1 32 2 1 32 3 1 32 4 1 32 5 1 32 6 1
		 32 7 1 24 33 1 25 33 1 26 33 1 27 33 1 28 33 1 29 33 1 30 33 1 31 33 1;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "SmallTree";
	rename -uid "0FA8E8AA-164F-0AF7-3ED2-449226BE4E60";
	setAttr ".rp" -type "double3" 143.08813056295202 397.71270014944184 -819.17452014812625 ;
	setAttr ".sp" -type "double3" 143.08813056295202 397.71270014944184 -819.17452014812625 ;
createNode mesh -n "SmallTreeShape" -p "SmallTree";
	rename -uid "C76A6CF7-3946-FB61-DE60-93A7902C2C29";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  255.73529 38.208099 -931.82166 
		143.08812 38.208099 -978.48157 30.440968 38.208099 -931.82166 -16.218929 38.208099 
		-819.1745 30.440968 38.208099 -706.52734 143.08812 38.208099 -659.86743 255.73531 
		38.208099 -706.52734 302.3952 38.208099 -819.1745 169.92284 757.21729 -846.00922 
		143.08813 757.21729 -857.12451 116.25342 757.21729 -846.00922 105.13812 757.21729 
		-819.1745 116.25342 757.21729 -792.33978 143.08813 757.21729 -781.22449 169.92284 
		757.21729 -792.33978 181.03815 757.21729 -819.1745 143.08813 38.208099 -819.1745 
		143.08813 757.21729 -819.1745;
	setAttr -s 18 ".vt[0:17]"  2.96830368 -1 -2.96830368 -2.2890517e-07 -1 -4.19781303
		 -2.96830368 -1 -2.96830368 -4.19781303 -1 -1.5275322e-07 -2.96830368 -1 2.96830368
		 -2.2890517e-07 -1 4.19781303 2.96830392 -1 2.96830392 4.19781303 -1 -1.5275322e-07
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BigTree";
	rename -uid "F7C5B834-C647-FB25-1835-979DFFFA1191";
	setAttr ".rp" -type "double3" -576.13795186641414 617.81054753897888 -1164.7083536555776 ;
	setAttr ".sp" -type "double3" -576.13795186641414 617.81054753897888 -1164.7083536555776 ;
createNode mesh -n "BigTreeShape" -p "BigTree";
	rename -uid "9A21E5F5-3E46-6AE9-74AC-A393EB409938";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  -463.49078 36.705475 -1277.3555 
		-576.13794 36.705475 -1324.0154 -688.7851 36.705475 -1277.3555 -735.44501 36.705475 
		-1164.7084 -688.7851 36.705475 -1052.0612 -576.13794 36.705475 -1005.4013 -463.49078 
		36.705475 -1052.0612 -416.8309 36.705475 -1164.7084 -549.30322 1198.9156 -1191.5431 
		-576.13794 1198.9156 -1202.6583 -602.97266 1198.9156 -1191.5431 -614.08795 1198.9156 
		-1164.7084 -602.97266 1198.9156 -1137.8737 -576.13794 1198.9156 -1126.7583 -549.30322 
		1198.9156 -1137.8737 -538.18793 1198.9156 -1164.7084 -576.13794 36.705475 -1164.7084 
		-576.13794 1198.9156 -1164.7084;
	setAttr -s 18 ".vt[0:17]"  2.96830368 -1 -2.96830368 -2.2890517e-07 -1 -4.19781303
		 -2.96830368 -1 -2.96830368 -4.19781303 -1 -1.5275322e-07 -2.96830368 -1 2.96830368
		 -2.2890517e-07 -1 4.19781303 2.96830392 -1 2.96830392 4.19781303 -1 -1.5275322e-07
		 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671 -0.99999988 1 0
		 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BranchSmallTree1";
	rename -uid "31B93615-2C42-3008-0567-308149C135F5";
	setAttr ".rp" -type "double3" -499.57318304364139 477.45228093317712 -1215.6201592036775 ;
	setAttr ".sp" -type "double3" -499.57318304364139 477.45228093317712 -1215.6201592036775 ;
createNode mesh -n "BranchSmallTree1Shape" -p "BranchSmallTree1";
	rename -uid "5ED173B4-6E4A-CC12-4F28-B7BD6CC3000C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  -582.39362 356.23996 -1208.249 
		-609.35431 381.10342 -1214.1951 -625.28296 408.5209 -1194.3445 -620.84875 422.43158 
		-1160.3256 -598.64917 414.68677 -1132.066 -571.68848 389.8233 -1126.12 -555.75983 
		362.40582 -1145.9706 -560.19409 348.49515 -1179.9895 -405.44461 558.00629 -1275.9878 
		-415.99417 567.73517 -1278.3143 -422.22693 578.46344 -1270.547 -420.49185 583.90662 
		-1257.2356 -411.80533 580.87616 -1246.1779 -401.25577 571.14722 -1243.8512 -395.02301 
		560.41895 -1251.6187 -396.75809 554.97577 -1264.9299 -590.52142 385.46335 -1170.1576 
		-408.62497 569.44122 -1261.0828;
	setAttr -s 18 ".vt[0:17]"  1.8071003 -1 -1.8071003 -9.2722544e-08 -1 -2.55562592
		 -1.8071003 -1 -1.8071003 -2.55562592 -1 -4.6361301e-08 -1.8071003 -1 1.8071003 -9.2722544e-08 -1 2.55562615
		 1.8071003 -1 1.8071003 2.55562615 -1 -4.6361301e-08 0.70710671 1 -0.70710671 0 1 -0.99999988
		 -0.70710671 1 -0.70710671 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994
		 0.70710677 1 0.70710677 1 1 0 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "TreetopSmall3";
	rename -uid "BC739374-854D-4ED5-2F90-0AAF75D3C705";
	setAttr ".rp" -type "double3" -338.44397541095998 671.97010821353581 -1243.1632810045603 ;
	setAttr ".sp" -type "double3" -338.44397541095998 671.97010821353581 -1243.1632810045603 ;
createNode mesh -n "TreetopSmall3Shape" -p "TreetopSmall3";
	rename -uid "60908818-8D43-3DA0-65C9-8B970AC6885F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 52 ".uvst[0].uvsp[0:51]" -type "float2" 0 0.2 0.125 0.2 0.25
		 0.2 0.375 0.2 0.5 0.2 0.625 0.2 0.75 0.2 0.875 0.2 1 0.2 0 0.40000001 0.125 0.40000001
		 0.25 0.40000001 0.375 0.40000001 0.5 0.40000001 0.625 0.40000001 0.75 0.40000001
		 0.875 0.40000001 1 0.40000001 0 0.60000002 0.125 0.60000002 0.25 0.60000002 0.375
		 0.60000002 0.5 0.60000002 0.625 0.60000002 0.75 0.60000002 0.875 0.60000002 1 0.60000002
		 0 0.80000001 0.125 0.80000001 0.25 0.80000001 0.375 0.80000001 0.5 0.80000001 0.625
		 0.80000001 0.75 0.80000001 0.875 0.80000001 1 0.80000001 0.0625 0 0.1875 0 0.3125
		 0 0.4375 0 0.5625 0 0.6875 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375
		 1 0.5625 1 0.6875 1 0.8125 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".pt[0:33]" -type "float3"  -276.50317 551.40247 -1305.1041 
		-338.44397 551.40247 -1330.7609 -400.38477 551.40247 -1305.1041 -426.0415 551.40247 
		-1243.1633 -400.38477 551.40247 -1181.2224 -338.44397 551.40247 -1155.5657 -276.50317 
		551.40247 -1181.2224 -250.84645 551.40247 -1243.1633 -238.22165 625.91736 -1343.3856 
		-338.44397 625.91736 -1384.899 -438.66629 625.91736 -1343.3856 -480.17975 625.91736 
		-1243.1633 -438.66629 625.91736 -1142.9409 -338.44397 625.91736 -1101.4275 -238.22163 
		625.91736 -1142.9409 -196.70818 625.91736 -1243.1633 -238.22165 718.02283 -1343.3856 
		-338.44397 718.02283 -1384.899 -438.66629 718.02283 -1343.3856 -480.17975 718.02283 
		-1243.1633 -438.66629 718.02283 -1142.9409 -338.44397 718.02283 -1101.4275 -238.22163 
		718.02283 -1142.9409 -196.70818 718.02283 -1243.1633 -276.50317 792.53778 -1305.1041 
		-338.44397 792.53778 -1330.7609 -400.38477 792.53778 -1305.1041 -426.0415 792.53778 
		-1243.1633 -400.38477 792.53778 -1181.2224 -338.44397 792.53778 -1155.5657 -276.50317 
		792.53778 -1181.2224 -250.84645 792.53778 -1243.1633 -338.44397 522.94025 -1243.1633 
		-338.44397 820.99994 -1243.1633;
	setAttr -s 34 ".vt[0:33]"  0.41562691 -0.809017 -0.415627 7.4505806e-09 -0.809017 -0.58778524
		 -0.41562688 -0.809017 -0.415627 -0.58778518 -0.809017 0 -0.41562688 -0.809017 0.415627
		 7.4505806e-09 -0.809017 0.58778548 0.41562691 -0.809017 0.415627 0.58778524 -0.809017 0
		 0.67249846 -0.309017 -0.67249823 7.4505806e-09 -0.309017 -0.95105648 -0.67249846 -0.309017 -0.67249823
		 -0.95105642 -0.309017 0 -0.67249846 -0.309017 0.6724987 7.4505806e-09 -0.309017 0.95105672
		 0.67249858 -0.309017 0.6724987 0.9510566 -0.309017 0 0.67249846 0.30901697 -0.67249823
		 7.4505806e-09 0.30901697 -0.95105648 -0.67249846 0.30901697 -0.67249823 -0.95105642 0.30901697 0
		 -0.67249846 0.30901697 0.6724987 7.4505806e-09 0.30901697 0.95105672 0.67249858 0.30901697 0.6724987
		 0.9510566 0.30901697 0 0.41562691 0.80901706 -0.415627 7.4505806e-09 0.80901706 -0.58778524
		 -0.41562688 0.80901706 -0.415627 -0.58778518 0.80901706 0 -0.41562688 0.80901706 0.415627
		 7.4505806e-09 0.80901706 0.58778548 0.41562691 0.80901706 0.415627 0.58778524 0.80901706 0
		 7.4505806e-09 -1.000000119209 0 7.4505806e-09 1 0;
	setAttr -s 72 ".ed[0:71]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1
		 7 0 1 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 16 17 1 17 18 1
		 18 19 1 19 20 1 20 21 1 21 22 1 22 23 1 23 16 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1
		 29 30 1 30 31 1 31 24 1 0 8 1 1 9 1 2 10 1 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 8 16 1
		 9 17 1 10 18 1 11 19 1 12 20 1 13 21 1 14 22 1 15 23 1 16 24 1 17 25 1 18 26 1 19 27 1
		 20 28 1 21 29 1 22 30 1 23 31 1 32 0 1 32 1 1 32 2 1 32 3 1 32 4 1 32 5 1 32 6 1
		 32 7 1 24 33 1 25 33 1 26 33 1 27 33 1 28 33 1 29 33 1 30 33 1 31 33 1;
	setAttr -s 40 -ch 144 ".fc[0:39]" -type "polyFaces" 
		f 4 -33 0 33 -9
		mu 0 4 9 0 1 10
		f 4 -34 1 34 -10
		mu 0 4 10 1 2 11
		f 4 -35 2 35 -11
		mu 0 4 11 2 3 12
		f 4 -36 3 36 -12
		mu 0 4 12 3 4 13
		f 4 -37 4 37 -13
		mu 0 4 13 4 5 14
		f 4 -38 5 38 -14
		mu 0 4 14 5 6 15
		f 4 -39 6 39 -15
		mu 0 4 15 6 7 16
		f 4 -40 7 32 -16
		mu 0 4 16 7 8 17
		f 4 -41 8 41 -17
		mu 0 4 18 9 10 19
		f 4 -42 9 42 -18
		mu 0 4 19 10 11 20
		f 4 -43 10 43 -19
		mu 0 4 20 11 12 21
		f 4 -44 11 44 -20
		mu 0 4 21 12 13 22
		f 4 -45 12 45 -21
		mu 0 4 22 13 14 23
		f 4 -46 13 46 -22
		mu 0 4 23 14 15 24
		f 4 -47 14 47 -23
		mu 0 4 24 15 16 25
		f 4 -48 15 40 -24
		mu 0 4 25 16 17 26
		f 4 -49 16 49 -25
		mu 0 4 27 18 19 28
		f 4 -50 17 50 -26
		mu 0 4 28 19 20 29
		f 4 -51 18 51 -27
		mu 0 4 29 20 21 30
		f 4 -52 19 52 -28
		mu 0 4 30 21 22 31
		f 4 -53 20 53 -29
		mu 0 4 31 22 23 32
		f 4 -54 21 54 -30
		mu 0 4 32 23 24 33
		f 4 -55 22 55 -31
		mu 0 4 33 24 25 34
		f 4 -56 23 48 -32
		mu 0 4 34 25 26 35
		f 3 -1 -57 57
		mu 0 3 1 0 36
		f 3 -2 -58 58
		mu 0 3 2 1 37
		f 3 -3 -59 59
		mu 0 3 3 2 38
		f 3 -4 -60 60
		mu 0 3 4 3 39
		f 3 -5 -61 61
		mu 0 3 5 4 40
		f 3 -6 -62 62
		mu 0 3 6 5 41
		f 3 -7 -63 63
		mu 0 3 7 6 42
		f 3 -8 -64 56
		mu 0 3 8 7 43
		f 3 24 65 -65
		mu 0 3 27 28 44
		f 3 25 66 -66
		mu 0 3 28 29 45
		f 3 26 67 -67
		mu 0 3 29 30 46
		f 3 27 68 -68
		mu 0 3 30 31 47
		f 3 28 69 -69
		mu 0 3 31 32 48
		f 3 29 70 -70
		mu 0 3 32 33 49
		f 3 30 71 -71
		mu 0 3 33 34 50
		f 3 31 64 -72
		mu 0 3 34 35 51;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "SmallTree1";
	rename -uid "A826DC69-7F44-8FFD-55AE-26B5791BAF0B";
	setAttr ".rp" -type "double3" 259.52720946576716 84.620857663352552 802.90151280049099 ;
	setAttr ".sp" -type "double3" 259.52720946576716 84.620857663352552 802.90151280049099 ;
createNode mesh -n "SmallTree1Shape" -p "SmallTree1";
	rename -uid "2962951D-6146-D328-40AB-D7AB0CDB7D78";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 10 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "bottom";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[8:15]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "e[0:7]";
	setAttr ".gtag[2].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "vtx[0:7]" "vtx[16]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "vtx[0:7]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "vtx[0:15]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "vtx[8:15]" "vtx[17]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 1 "vtx[8:15]";
	setAttr ".gtag[7].gtagnm" -type "string" "sides";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 1 "f[0:7]";
	setAttr ".gtag[8].gtagnm" -type "string" "top";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[16:23]";
	setAttr ".gtag[9].gtagnm" -type "string" "topRing";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "e[8:15]";
	setAttr ".pv" -type "double2" 0.5 0.5000000074505806 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.6875 0.40625 0.6875 0.4375 0.6875 0.46875 0.6875 0.5 0.6875 0.53125 0.6875
		 0.5625 0.6875 0.59375 0.6875 0.625 0.6875 0.61048543 0.73326457 0.5 0.6875 0.38951457
		 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543 0.65625
		 0.84375 0.5 0.15625 0.5 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".pt[0:17]" -type "float3"  298.37445 42.044228 764.05426 
		259.52722 42.044228 747.96326 220.67996 42.044228 764.05426 204.58894 42.044228 802.90149 
		220.67996 42.044228 841.74878 259.52722 42.044228 857.83978 298.37445 42.044228 841.74878 
		314.46548 42.044228 802.90149 279.80557 127.19749 782.62311 259.52722 127.19749 774.22357 
		239.24883 127.19749 782.62311 230.84924 127.19749 802.90149 239.24883 127.19749 823.17987 
		259.52722 127.19749 831.57947 279.8056 127.19749 823.17987 288.20517 127.19749 802.90149 
		259.52722 42.044228 802.90149 259.52722 127.19749 802.90149;
	setAttr -s 18 ".vt[0:17]"  2.96830368 -1 -2.96830368 -2.2890517e-07 -1 -4.19781303
		 -2.96830368 -1 -2.96830368 -4.19781303 -1 -1.5275322e-07 -2.96830368 -1 2.96830368
		 -2.2890517e-07 -1 4.19781303 2.96830392 -1 2.96830392 4.19781303 -1 -1.5275322e-07
		 1.54946303 1 -1.54946339 -7.1005402e-08 1 -2.19127274 -1.54946363 1 -1.54946339 -2.19127274 1 -3.550268e-08
		 -1.54946363 1 1.54946339 -7.1005402e-08 1 2.19127226 1.54946351 1 1.54946351 2.19127297 1 -3.550268e-08
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 1 9 10 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 8 1 0 8 1 1 9 1 2 10 1
		 3 11 1 4 12 1 5 13 1 6 14 1 7 15 1 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "axe";
	rename -uid "33A70224-5045-FDFB-5451-52A8335FAF16";
	setAttr ".t" -type "double3" -247.44784603752021 -57.438232634171072 107.19871625963503 ;
	setAttr ".r" -type "double3" 2.1339458769996957 29.82280394671033 5.060560471502586 ;
	setAttr ".rp" -type "double3" 598.03831138257669 223.80545839900384 661.04570395563462 ;
	setAttr ".sp" -type "double3" 598.03831138257669 223.80545839900384 661.04570395563462 ;
createNode transform -n "polySurface6" -p "axe";
	rename -uid "C0B054DD-DB46-38E3-FA58-A3897435B712";
	setAttr ".rp" -type "double3" 1.1368683772161603e-13 1.4210854715202004e-14 -2.8421709430404007e-14 ;
	setAttr ".sp" -type "double3" 1.1368683772161603e-13 1.4210854715202004e-14 -2.8421709430404007e-14 ;
createNode mesh -n "polySurfaceShape15" -p "polySurface6";
	rename -uid "0D90FC43-7249-CCAC-7FF0-B78ED9D45CE9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 14 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 8 "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]";
	setAttr ".gtag[2].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 8 "e[0]" "e[1]" "e[2]" "e[3]" "e[4]" "e[5]" "e[6]" "e[7]";
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 9 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]" "vtx[16]";
	setAttr ".gtag[4].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 8 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]";
	setAttr ".gtag[5].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 16 "vtx[0]" "vtx[1]" "vtx[2]" "vtx[3]" "vtx[4]" "vtx[5]" "vtx[6]" "vtx[7]" "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]";
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 9 "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]" "vtx[17]";
	setAttr ".gtag[7].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 8 "vtx[8]" "vtx[9]" "vtx[10]" "vtx[11]" "vtx[12]" "vtx[13]" "vtx[14]" "vtx[15]";
	setAttr ".gtag[8].gtagnm" -type "string" "front";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[9].gtagnm" -type "string" "left";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[10].gtagnm" -type "string" "right";
	setAttr ".gtag[10].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[11].gtagnm" -type "string" "sides";
	setAttr ".gtag[11].gtagcmp" -type "componentList" 8 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]";
	setAttr ".gtag[12].gtagnm" -type "string" "top";
	setAttr ".gtag[12].gtagcmp" -type "componentList" 8 "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]";
	setAttr ".gtag[13].gtagnm" -type "string" "topRing";
	setAttr ".gtag[13].gtagcmp" -type "componentList" 8 "e[8]" "e[9]" "e[10]" "e[11]" "e[12]" "e[13]" "e[14]" "e[15]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.375 0.3125 0.40625
		 0.3125 0.40625 0.6875 0.375 0.6875 0.4375 0.3125 0.4375 0.6875 0.46875 0.3125 0.46875
		 0.6875 0.5 0.3125 0.5 0.6875 0.53125 0.3125 0.53125 0.6875 0.5625 0.3125 0.5625 0.6875
		 0.59375 0.3125 0.59375 0.6875 0.625 0.3125 0.625 0.6875 0.5 1.4901161e-08 0.61048543
		 0.04576458 0.5 0.15625 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543
		 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.61048543 0.95423543 0.5 1 0.5
		 0.84375 0.38951457 0.95423543 0.34375 0.84375 0.38951457 0.73326457 0.5 0.6875 0.61048543
		 0.73326457 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  517.8293457 249.72128296 656.2442627 517.8293457 251.50318909 660.54614258
		 517.8293457 249.72128296 664.84802246 517.8293457 245.41938782 666.62994385 517.8293457 241.11749268 664.84802246
		 517.8293457 239.33558655 660.54614258 517.8293457 241.11749268 656.2442627 517.8293457 245.41938782 654.46234131
		 717.017211914 249.72128296 656.2442627 717.017211914 251.50318909 660.54614258 717.017211914 249.72128296 664.84802246
		 717.017211914 245.41938782 666.62994385 717.017211914 241.11749268 664.84802246 717.017211914 239.33558655 660.54614258
		 717.017211914 241.11749268 656.2442627 717.017211914 245.41938782 654.46234131 517.8293457 245.41938782 660.54614258
		 717.017211914 245.41938782 660.54614258;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 0 1 2 3
		f 4 1 18 -10 -18
		mu 0 4 1 4 5 2
		f 4 2 19 -11 -19
		mu 0 4 4 6 7 5
		f 4 3 20 -12 -20
		mu 0 4 6 8 9 7
		f 4 4 21 -13 -21
		mu 0 4 8 10 11 9
		f 4 5 22 -14 -22
		mu 0 4 10 12 13 11
		f 4 6 23 -15 -23
		mu 0 4 12 14 15 13
		f 4 7 16 -16 -24
		mu 0 4 14 16 17 15
		f 3 -1 -25 25
		mu 0 3 18 19 20
		f 3 -2 -26 26
		mu 0 3 21 18 20
		f 3 -3 -27 27
		mu 0 3 22 21 20
		f 3 -4 -28 28
		mu 0 3 23 22 20
		f 3 -5 -29 29
		mu 0 3 24 23 20
		f 3 -6 -30 30
		mu 0 3 25 24 20
		f 3 -7 -31 31
		mu 0 3 26 25 20
		f 3 -8 -32 24
		mu 0 3 19 26 20
		f 3 8 33 -33
		mu 0 3 27 28 29
		f 3 9 34 -34
		mu 0 3 28 30 29
		f 3 10 35 -35
		mu 0 3 30 31 29
		f 3 11 36 -36
		mu 0 3 31 32 29
		f 3 12 37 -37
		mu 0 3 32 33 29
		f 3 13 38 -38
		mu 0 3 33 34 29
		f 3 14 39 -39
		mu 0 3 34 35 29
		f 3 15 32 -40
		mu 0 3 35 27 29;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "polySurface7" -p "axe";
	rename -uid "BFD652CB-7046-4264-7EBF-32925618AF72";
	setAttr ".rp" -type "double3" 504.94415283203148 223.80546569824224 661.04571533203136 ;
	setAttr ".sp" -type "double3" 504.94415283203148 223.80546569824224 661.04571533203136 ;
createNode mesh -n "polySurfaceShape16" -p "polySurface7";
	rename -uid "2544C361-0A4D-88BD-8F0C-E3B9B56B239B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 14 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "bottomRing";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[3].gtagnm" -type "string" "cylBottomCap";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[4].gtagnm" -type "string" "cylBottomRing";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[5].gtagnm" -type "string" "cylSides";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[6].gtagnm" -type "string" "cylTopCap";
	setAttr ".gtag[6].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[7].gtagnm" -type "string" "cylTopRing";
	setAttr ".gtag[7].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[8].gtagnm" -type "string" "front";
	setAttr ".gtag[8].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[9].gtagnm" -type "string" "left";
	setAttr ".gtag[9].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[10].gtagnm" -type "string" "right";
	setAttr ".gtag[10].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[11].gtagnm" -type "string" "sides";
	setAttr ".gtag[11].gtagcmp" -type "componentList" 0;
	setAttr ".gtag[12].gtagnm" -type "string" "top";
	setAttr ".gtag[12].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".gtag[13].gtagnm" -type "string" "topRing";
	setAttr ".gtag[13].gtagcmp" -type "componentList" 0;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  479.059387207 193.76693726 660.013427734
		 530.82891846 193.76693726 660.013427734 479.059387207 193.76693726 662.07800293 530.82891846 193.76693726 662.07800293
		 479.05947876 253.84399414 669.57568359 530.82879639 253.84399414 669.57568359 479.05947876 253.84399414 652.51574707
		 530.82879639 253.84399414 652.51574707;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 4 5
		f 4 2 9 -4 -9
		mu 0 4 5 4 6 7
		f 4 3 11 -1 -11
		mu 0 4 7 6 8 9
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 2
		f 4 10 4 6 8
		mu 0 4 12 0 3 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "paws";
	rename -uid "64CDE36A-AC4E-AD08-BF8B-04B43CFF8BEE";
	setAttr ".rp" -type "double3" 1338.318337586742 117.74562963303669 674.00430704537996 ;
	setAttr ".sp" -type "double3" 1338.318337586742 117.74562963303669 674.00430704537996 ;
createNode mesh -n "pawsShape" -p "paws";
	rename -uid "1B2C246A-C945-133D-A5A9-E2A36605E761";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "f[5]" "f[11]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[10]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 28 ".uvst[0].uvsp[0:27]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  1390.38256836 42.63889313 663.78015137 1413.58813477 71.44792938 648.56347656
		 1284.98291016 164.043334961 732.89489746 1308.18823242 192.85237122 717.67822266
		 1263.048583984 164.043334961 699.44506836 1286.25402832 192.85237122 684.22839355
		 1368.44836426 42.63889313 630.33044434 1391.65380859 71.44792938 615.11376953 1280.61303711 81.51656342 735.76031494
		 1300.59448242 49.43749237 722.65771484 1397.9765625 186.053771973 658.80065918 1417.95788574 153.97470093 645.69799805
		 1376.042236328 186.053771973 625.35083008 1396.02355957 153.97470093 612.24829102
		 1258.67871094 81.51656342 702.31054688 1278.66015625 49.43749237 689.20800781;
	setAttr -s 24 ".ed[0:23]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0;
	setAttr -s 12 -ch 48 ".fc[0:11]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 4 5
		f 4 2 9 -4 -9
		mu 0 4 5 4 6 7
		f 4 3 11 -1 -11
		mu 0 4 7 6 8 9
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 2
		f 4 10 4 6 8
		mu 0 4 12 0 3 13
		f 4 12 17 -14 -17
		mu 0 4 14 15 16 17
		f 4 13 19 -15 -19
		mu 0 4 17 16 18 19
		f 4 14 21 -16 -21
		mu 0 4 19 18 20 21
		f 4 15 23 -13 -23
		mu 0 4 21 20 22 23
		f 4 -24 -22 -20 -18
		mu 0 4 15 24 25 16
		f 4 22 16 18 20
		mu 0 4 26 14 17 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "paws2";
	rename -uid "5FE895B8-EB4C-9080-F7F5-118C3431D693";
	setAttr ".rp" -type "double3" 1174.6169078343983 117.74562963303669 424.35993634477393 ;
	setAttr ".sp" -type "double3" 1174.6169078343983 117.74562963303669 424.35993634477393 ;
createNode mesh -n "paws2Shape" -p "paws2";
	rename -uid "435FBC1E-9A4D-72EE-1C4C-13BC5C9257FB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 2 "f[2]" "f[8]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 2 "f[3]" "f[9]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 2 "f[0]" "f[6]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 2 "f[5]" "f[11]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 2 "f[4]" "f[10]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 2 "f[1]" "f[7]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 28 ".uvst[0].uvsp[0:27]" -type "float2" 0.375 0 0.625 0 0.625
		 0.25 0.375 0.25 0.625 0.5 0.375 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.375 0 0.625 0 0.625 0.25 0.375 0.25 0.625 0.5 0.375
		 0.5 0.625 0.75 0.375 0.75 0.625 1 0.375 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".vt[0:15]"  1226.68115234 42.63889313 414.13580322 1249.88671875 71.44792938 398.91912842
		 1121.28149414 164.043334961 483.25048828 1144.48681641 192.85237122 468.033813477
		 1099.34716797 164.043334961 449.80075073 1122.55249023 192.85237122 434.58407593
		 1204.74682617 42.63889313 380.68606567 1227.95239258 71.44792938 365.46939087 1116.91162109 81.51656342 486.11593628
		 1136.89306641 49.43749237 473.013366699 1234.27514648 186.053771973 409.15625 1254.25646973 153.97470093 396.053649902
		 1212.34082031 186.053771973 375.70651245 1232.32214355 153.97470093 362.60391235
		 1094.97729492 81.51656342 452.66619873 1114.95874023 49.43749237 439.56362915;
	setAttr -s 24 ".ed[0:23]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 10 11 0 12 13 0 14 15 0 8 10 0 9 11 0 10 12 0
		 11 13 0 12 14 0 13 15 0 14 8 0 15 9 0;
	setAttr -s 12 -ch 48 ".fc[0:11]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 4 5
		f 4 2 9 -4 -9
		mu 0 4 5 4 6 7
		f 4 3 11 -1 -11
		mu 0 4 7 6 8 9
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 2
		f 4 10 4 6 8
		mu 0 4 12 0 3 13
		f 4 12 17 -14 -17
		mu 0 4 14 15 16 17
		f 4 13 19 -15 -19
		mu 0 4 17 16 18 19
		f 4 14 21 -16 -21
		mu 0 4 19 18 20 21
		f 4 15 23 -13 -23
		mu 0 4 21 20 22 23
		f 4 -24 -22 -20 -18
		mu 0 4 15 24 25 16
		f 4 22 16 18 20
		mu 0 4 26 14 17 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table";
	rename -uid "EBB46BEB-AC4A-B224-24D9-81BED7EDB3AC";
	setAttr ".rp" -type "double3" 807.73995049244377 276.97162430856838 -503.84773494821559 ;
	setAttr ".sp" -type "double3" 807.73995049244377 276.97162430856838 -503.84773494821559 ;
createNode mesh -n "tableShape" -p "table";
	rename -uid "A464BB55-EB4D-546A-5EF0-31AE71CF4030";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  744.71198 265.30502 -342.86588 
		870.76788 265.30502 -342.86588 744.71198 288.63824 -342.86588 870.76788 288.63824 
		-342.86588 744.71198 288.63824 -664.82959 870.76788 288.63824 -664.82959 744.71198 
		265.30502 -664.82959 870.76788 265.30502 -664.82959;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table";
	rename -uid "891370E8-4246-FC3E-63AF-6CA7B08A5919";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table1";
	rename -uid "6F76B702-3C42-6FE7-DA85-57B1D86CA89C";
	setAttr ".rp" -type "double3" 851.07146009734106 227.03737698082134 -641.07249535508743 ;
	setAttr ".sp" -type "double3" 851.07146009734106 227.03737698082134 -641.07249535508743 ;
createNode mesh -n "table1Shape" -p "table1";
	rename -uid "2062FF78-A548-C19E-7015-0EACD33FD70F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  841.01202 282.5119 -629.87701 
		861.13092 282.5119 -629.87701 841.01202 282.45413 -653.26801 861.13092 282.45413 
		-653.26801 841.01202 171.56285 -652.26801 861.13092 171.56285 -652.26801 841.01202 
		171.62062 -628.87701 861.13092 171.62062 -628.87701;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table1";
	rename -uid "53FC0430-7E4B-DD4B-4E57-8C9DF82C3FFC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table2";
	rename -uid "D5033CED-EC40-C89F-05A6-00AE033FB5B2";
	setAttr ".rp" -type "double3" 769.25502425079412 229.1996005464261 -642.19265292586374 ;
	setAttr ".sp" -type "double3" 769.25502425079412 229.1996005464261 -642.19265292586374 ;
createNode mesh -n "table2Shape" -p "table2";
	rename -uid "66BF1DAD-FB46-BEDD-A04E-F9858C213409";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  759.19556 284.67413 -630.99713 
		779.31451 284.67413 -630.99713 759.19556 284.61636 -654.38812 779.31451 284.61636 
		-654.38812 759.19556 173.72507 -653.38812 779.31451 173.72507 -653.38812 759.19556 
		173.78284 -629.99713 779.31451 173.78284 -629.99713;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table2";
	rename -uid "3BD4F9DA-0A48-2403-2843-A3B131F03917";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table3";
	rename -uid "12D91B0D-7145-E682-FE05-EAB413D792EF";
	setAttr ".rp" -type "double3" 764.19293696658542 227.03737698082134 -375.83577393592321 ;
	setAttr ".sp" -type "double3" 764.19293696658542 227.03737698082134 -375.83577393592321 ;
createNode mesh -n "table3Shape" -p "table3";
	rename -uid "FCF0D367-DE40-6A96-236D-F9B9370C4A6A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  754.13348 282.5119 -364.64029 
		774.25238 282.5119 -364.64029 754.13348 282.45413 -388.03128 774.25238 282.45413 
		-388.03128 754.13348 171.56285 -387.03128 774.25238 171.56285 -387.03128 754.13348 
		171.62062 -363.64029 774.25238 171.62062 -363.64029;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table3";
	rename -uid "899F3D75-9A40-1BB3-1F5F-8DA1CAECE0BB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "table4";
	rename -uid "0F528764-8C45-9D90-A7B0-DC937E333598";
	setAttr ".rp" -type "double3" 848.5135701048431 227.03737698082134 -375.31551484618598 ;
	setAttr ".sp" -type "double3" 848.5135701048431 227.03737698082134 -375.31551484618598 ;
createNode mesh -n "table4Shape" -p "table4";
	rename -uid "4FCB8D4E-0D4F-0442-4422-FEB3AFB9E447";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.39749997854232788 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  838.4541 282.5119 -364.12003 
		858.57306 282.5119 -364.12003 838.4541 282.45413 -387.51102 858.57306 282.45413 -387.51102 
		838.4541 171.56285 -386.51102 858.57306 171.56285 -386.51102 838.4541 171.62062 -363.12003 
		858.57306 171.62062 -363.12003;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.028887421 0.5 0.5 -0.028887421 0.5
		 -0.5 0.028887421 0.5 0.5 0.028887421 0.5 -0.5 0.028887421 -0.5 0.5 0.028887421 -0.5
		 -0.5 -0.028887421 -0.5 0.5 -0.028887421 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 0 2 0 1 3 0 4 6 0 5 7 0 6 0 0
		 4 5 0 6 7 0 3 5 0 2 4 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -2 -3 0 3
		mu 0 4 3 2 0 1
		f 4 1 9 -8 -11
		mu 0 4 2 3 5 4
		f 4 -5 7 5 -9
		mu 0 4 6 4 5 7
		f 4 10 4 6 2
		mu 0 4 2 13 12 0
		f 4 -1 -7 8 11
		mu 0 4 9 8 6 7
		f 4 -6 -10 -4 -12
		mu 0 4 10 11 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "polySurfaceShape7" -p "table4";
	rename -uid "A561B57F-3941-3FA1-A39E-51B054B74F5D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "e[0:3]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.47111258 0 0 0.47111258 
		0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 -0.47111258 0 0 0.47111258 0 
		0 0.47111258 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "D172E702-D645-E53D-83C0-509D58CCDA8E";
	setAttr -s 25 ".lnk";
	setAttr -s 25 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E1B46E95-A444-99DA-FC36-5E8290DF1D88";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "60A9D49F-5A48-0008-72DB-0F95F98132BB";
createNode displayLayerManager -n "layerManager";
	rename -uid "F5F4E573-1F47-F7DF-2AD4-ED95F2669831";
createNode displayLayer -n "defaultLayer";
	rename -uid "F889A05C-014D-3B5D-AD75-1F92B0BECC5B";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "15B69B25-E242-AD49-7D69-8A8371AC9CA0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "F11D54A8-4447-1F20-A58B-C7A67CABC21B";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "A2D2E1E1-D648-7251-322D-6995CBB9BDDC";
	setAttr ".version" -type "string" "5.2.0";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "86F5A765-AB40-539D-1CD6-DE931906D92A";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "E45E70BA-7544-CBC7-FAE6-97ADA9C14B50";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "1116ACFD-E741-218C-380A-D691EB64E8E4";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "A8941C40-D447-DCA6-AAD1-E895497D73DC";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 100\n            -height 30\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1119\n            -height 670\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n"
		+ "            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n"
		+ "            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n"
		+ "                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n"
		+ "                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n"
		+ "                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1119\\n    -height 670\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1119\\n    -height 670\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "A6C36C6A-4A4E-B5AF-C537-EEAA0D1E3B15";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCylinder -n "polyCylinder4";
	rename -uid "6B27B152-B044-35B9-804C-6C9BB8B80E53";
	setAttr ".sa" 10;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode groupId -n "groupId8";
	rename -uid "61C9F3B7-3D44-EB04-49DA-BFAE2E289478";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "2BD43C56-5D48-82CC-DD84-FDB638BA6E73";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "ED89DA51-C441-0EC1-8A75-28B51733A67C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "9BCCFDE5-8842-5E7E-BCC4-01B7D447716C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "3A68F5FC-CB43-3395-7C73-D8850134F1F9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "BF8A9109-7D4A-EB77-E298-2C8D47EAE1A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId13";
	rename -uid "53F62F72-8440-A3B2-C7DF-7C87AC120C8D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "A774178D-BD47-D4F0-6963-40835A71CD16";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "verde2";
	rename -uid "B9750F8F-8540-8668-BADB-479A232830B0";
	setAttr ".base_color" -type "float3" 0 0.37799999 0 ;
createNode shadingEngine -n "aiStandardSurface1SG";
	rename -uid "88F4583D-AC49-36F2-1DDA-77AACCB8C6F8";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "933AD207-7043-231F-4BE2-A5ADF6C3A35F";
createNode aiStandardSurface -n "verde1";
	rename -uid "B2BEAADA-A348-1E89-EA0C-B99948ABA471";
	setAttr ".base_color" -type "float3" 0.0070000002 0.3581 0.0070000002 ;
createNode shadingEngine -n "aiStandardSurface2SG";
	rename -uid "AA8851FC-F341-93A8-81BE-D897AD959932";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "83A4FA5E-3D48-AF10-EA73-049430E11B8C";
createNode aiStandardSurface -n "marron";
	rename -uid "11538EB2-8F45-35A0-B955-B6B31381B4A7";
	setAttr ".base_color" -type "float3" 0.124 0.066067271 0.016988004 ;
createNode shadingEngine -n "aiStandardSurface3SG";
	rename -uid "2306FA81-D846-96D4-F1DE-8D82E17F00B8";
	setAttr ".ihi" 0;
	setAttr -s 6 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "3CE2620F-0148-90A0-A974-7DA59485A3FC";
createNode aiStandardHair -n "aiStandardHair1";
	rename -uid "3399B5C0-7542-3D51-E0A6-918E9FC9E326";
createNode shadingEngine -n "aiStandardHair1SG";
	rename -uid "0A3F2A2F-754A-4802-56D4-81B1C301A9BD";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "AB84B206-4E4E-C302-FEFC-F89A4D85C9C1";
createNode aiStandardSurface -n "aiStandardSurface4";
	rename -uid "756C3819-3948-F080-F34B-AEB6B5A18C18";
	setAttr ".base_color" -type "float3" 0.27700001 0.17692837 0.092241004 ;
createNode shadingEngine -n "aiStandardSurface4SG";
	rename -uid "0A06D897-1C4E-ECF5-A80B-82A1E71109B2";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
	rename -uid "65167214-E545-1B9F-525A-BABA8EF52575";
createNode aiStandardSurface -n "marron_claro";
	rename -uid "FA3A9A0B-CE48-A1AC-A250-D78BE945E03E";
	setAttr ".base_color" -type "float3" 0.27700001 0.17692837 0.092241004 ;
createNode shadingEngine -n "aiStandardSurface5SG";
	rename -uid "DA4D4B72-CF47-2277-6086-949D134E4856";
	setAttr ".ihi" 0;
	setAttr -s 14 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo6";
	rename -uid "8167FB6A-4646-926A-BAA9-51B6664F422C";
createNode aiStandardSurface -n "verde_claro";
	rename -uid "06339AF2-A342-0CEB-62D6-DBB69E1C25A8";
	setAttr ".base_color" -type "float3" 0 1 0 ;
createNode shadingEngine -n "aiStandardSurface6SG";
	rename -uid "CCD4F4B7-5A4E-9B72-FD39-FD91342AE40D";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo7";
	rename -uid "3BBF0BD4-3F4F-D290-1303-D1937AADB3D3";
createNode aiStandardSurface -n "marronClaro";
	rename -uid "9E7719E9-9143-95BB-A8E8-91894B6C802F";
	setAttr ".base_color" -type "float3" 0.5783 0.4465 0.22669999 ;
createNode shadingEngine -n "aiStandardSurface7SG";
	rename -uid "BEC5C0B0-FA4A-6F54-F87E-459BB46F125E";
	setAttr ".ihi" 0;
	setAttr -s 30 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 16 ".gn";
createNode materialInfo -n "materialInfo8";
	rename -uid "9A316835-8047-0DB1-6476-F3BE111894EF";
createNode aiStandardSurface -n "MarronMuyClaro";
	rename -uid "C9DED39B-E640-9E8C-92D0-24A955EFD63C";
	setAttr ".base_color" -type "float3" 1 0.9070372 0.75199997 ;
createNode shadingEngine -n "aiStandardSurface8SG";
	rename -uid "ACD0E6A1-8740-A609-D4D4-778035355E12";
	setAttr ".ihi" 0;
	setAttr -s 10 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo9";
	rename -uid "91559BE3-9242-0196-D27A-7197FF07D978";
createNode aiStandardSurface -n "Rojo";
	rename -uid "7AF889E8-ED40-BF93-546A-229A19594136";
	setAttr ".base_color" -type "float3" 0.5043 0.069200002 0.069200002 ;
createNode shadingEngine -n "aiStandardSurface9SG";
	rename -uid "B808CB87-2344-AD14-B6BA-BE819144CC5C";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
createNode materialInfo -n "materialInfo10";
	rename -uid "5845D34F-A64B-E362-5527-58877EED997D";
createNode groupId -n "groupId48";
	rename -uid "BE70F373-9546-2001-267F-0ABA48F15138";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "azulClaro";
	rename -uid "E332B20F-9344-D30A-FB59-178AD4F78EB4";
	setAttr ".base_color" -type "float3" 0.55326307 0.55326307 0.82700002 ;
	setAttr ".metalness" 0.61594200134277344;
createNode shadingEngine -n "aiStandardSurface10SG";
	rename -uid "36B4A447-5343-8852-905B-F5A7A6693587";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo11";
	rename -uid "0FF87D26-CB48-EED7-DDF2-2280EB4C3146";
createNode groupId -n "groupId51";
	rename -uid "AB2EECEB-4B4F-3A71-1478-19BE3872B8DA";
	setAttr ".ihi" 0;
createNode aiShadowMatte -n "aiShadowMatte1";
	rename -uid "7D653E9A-7D4B-E30A-054B-95A4E646E048";
createNode shadingEngine -n "aiShadowMatte1SG";
	rename -uid "7C39F56F-B348-860A-B2F6-58A3B6433D85";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo12";
	rename -uid "AB9AE88F-AD4C-8479-34FC-7DAD44E0162A";
createNode aiStandardSurface -n "aiStandardSurface11";
	rename -uid "C9A6316E-0A4B-9FEC-371C-2E94A2A3FC3C";
createNode shadingEngine -n "aiStandardSurface11SG";
	rename -uid "C1386150-C443-20BC-B775-D59B5E7290B0";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo13";
	rename -uid "80187DB5-C64F-F29D-4B8F-A8A07C017786";
createNode aiStandardSurface -n "amarillo";
	rename -uid "D7D4269B-8E41-B63D-8AD7-4C98A7D00897";
	setAttr ".base_color" -type "float3" 1 1 0 ;
createNode shadingEngine -n "aiStandardSurface12SG";
	rename -uid "F259AB4F-0A43-A7B9-A036-6BB12A4C95EC";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo14";
	rename -uid "68156581-6548-6CA2-8FA1-34869F5EA010";
createNode groupId -n "groupId54";
	rename -uid "EA6E8B7E-C64B-3497-FA3E-038C24CC37E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId55";
	rename -uid "45ACEEC7-584B-82FE-CB0D-DEB336E17207";
	setAttr ".ihi" 0;
createNode groupId -n "groupId56";
	rename -uid "6F81C5CC-EB44-73D9-0A9A-E5ACDCBA6517";
	setAttr ".ihi" 0;
createNode groupId -n "groupId57";
	rename -uid "3EC11368-F04B-2E21-F585-689300340FC6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "3DA1E253-4742-A8FF-8750-6AA12760F560";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "290C762A-DC4B-1E5A-E0C1-FAB5B0F9AC1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "35BAB460-BC4F-60A6-5C73-EFB16FCA2ACA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "A5DF3078-614D-5D63-D546-A098773D0891";
	setAttr ".ihi" 0;
createNode groupId -n "groupId62";
	rename -uid "57E0B783-784E-7BD3-53C5-8C95A042A059";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "9548DEA8-F54E-95D4-70EB-148C96F98788";
	setAttr ".ihi" 0;
createNode groupId -n "groupId65";
	rename -uid "7D84C69C-774F-4A23-210C-44B2CD1B821D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "11EA6DC5-5345-7115-FD57-C0894A6E3A87";
	setAttr ".ihi" 0;
createNode groupId -n "groupId70";
	rename -uid "40D28AFD-BE4D-173D-19FF-CF82DB051F8E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId71";
	rename -uid "EF4AD8CD-A543-0F0E-4BA4-6EB5BA6F5D35";
	setAttr ".ihi" 0;
createNode groupId -n "groupId72";
	rename -uid "D84818F1-BE4B-C016-CDEC-18A27465D475";
	setAttr ".ihi" 0;
createNode groupId -n "groupId79";
	rename -uid "E3A1240D-4F4B-E6B4-E723-6494EE99ADBC";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "negro";
	rename -uid "DC36C68F-ED48-A59D-8FCE-84A0DB186876";
	setAttr ".base_color" -type "float3" 0 0 0 ;
createNode shadingEngine -n "aiStandardSurface13SG";
	rename -uid "336D6CDE-CC4B-ED42-9B3C-C4A7DE9A6A72";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 4 ".gn";
createNode materialInfo -n "materialInfo15";
	rename -uid "DCE7783D-DF4A-A9E1-3B12-37BFDD967E3D";
createNode groupId -n "groupId81";
	rename -uid "551F8F9F-9A48-7033-02A8-E991476429BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId82";
	rename -uid "4975095B-2D4A-E82B-3DAE-0AB5711CF596";
	setAttr ".ihi" 0;
createNode groupId -n "groupId83";
	rename -uid "51176213-264B-6D9A-2DB4-AAB87F6EF59F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId84";
	rename -uid "AC07C83E-8946-8F70-2B05-70AB15D3CE33";
	setAttr ".ihi" 0;
createNode groupId -n "groupId85";
	rename -uid "62189BC1-3F45-B7DA-8BEB-1AB9BC070BBC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId86";
	rename -uid "2E768B2D-A441-8419-ED79-FA9EEAAB52F9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId80";
	rename -uid "92FA7050-984C-8AF4-CCB1-BA821CE4C0B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId78";
	rename -uid "2FA1050D-4849-CF30-6ED9-44A41E685F68";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "amarilloClaro";
	rename -uid "F2A78E88-3C40-CA39-23E2-368EEF1A530D";
	setAttr ".base_color" -type "float3" 1 1 0.759 ;
createNode shadingEngine -n "aiStandardSurface14SG";
	rename -uid "88D9B080-DE4E-E6F7-6CF0-86A3EF87B6E6";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo16";
	rename -uid "578F8EB7-DD43-8846-848B-0287E408A42A";
createNode groupId -n "groupId88";
	rename -uid "92A3AA89-EE4E-2008-E061-C9B77F6FEA5C";
	setAttr ".ihi" 0;
createNode aiStandardSurface -n "morado";
	rename -uid "82AB7FE5-D24D-B4CC-F4FA-20AF7B8A4F3C";
	setAttr ".base_color" -type "float3" 0.34627929 0.227835 0.41499999 ;
createNode shadingEngine -n "aiStandardSurface15SG";
	rename -uid "65DE91B7-B64A-94BF-33F4-6A9694168524";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo17";
	rename -uid "827FDDD4-5442-D1DD-09EF-94B908DF1336";
createNode aiStandardSurface -n "aiStandardSurface16";
	rename -uid "08F3F557-C845-C159-F598-3C8DC8114D34";
createNode shadingEngine -n "aiStandardSurface16SG";
	rename -uid "A164D8CA-F141-FC09-4E15-EFBBB3A699FA";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo18";
	rename -uid "BB46A6D8-7A4C-285F-9E69-5AB9F3CD0328";
createNode aiStandardSurface -n "aiStandardSurface17";
	rename -uid "2DBB01D8-C24A-1265-CAD4-11A92182DE4B";
createNode shadingEngine -n "aiStandardSurface17SG";
	rename -uid "886145D7-A645-9DF9-0701-1EBBE1ED6B29";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo19";
	rename -uid "27929A74-B744-A21C-5DB3-C98248C28D64";
createNode aiStandardSurface -n "moradoClaro";
	rename -uid "5F56EC96-BE4B-9FAC-FCD5-94B3B49F2EC3";
	setAttr ".base_color" -type "float3" 1 0.759 0.98400003 ;
createNode shadingEngine -n "aiStandardSurface18SG";
	rename -uid "E1C007B6-2545-D335-9794-64B9EC7B9AEF";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo20";
	rename -uid "F88FB19F-B94A-6185-EA34-C7969D0ED611";
createNode aiStandardSurface -n "gris";
	rename -uid "FFB9C8CC-BD4B-ADBA-DE60-0DAF5D2708B0";
	setAttr ".base_color" -type "float3" 0.34099999 0.34099999 0.34099999 ;
createNode shadingEngine -n "aiStandardSurface19SG";
	rename -uid "795C1B73-7745-D89A-70D5-61BE52B45567";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo21";
	rename -uid "3E00B22B-2641-2171-A337-D6B0391FD0E0";
createNode aiStandardSurface -n "aiStandardSurface20";
	rename -uid "4B6DE1B3-6B4A-E964-472A-27923204A27A";
createNode shadingEngine -n "aiStandardSurface20SG";
	rename -uid "5359C940-6F47-F439-29A7-179A7FFCC121";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo22";
	rename -uid "3088A09F-CB4A-973C-F77F-97B53678D4ED";
createNode file -n "file1";
	rename -uid "16BB3333-BF49-CE3C-39F7-CDB8CD535C2D";
	setAttr ".ftn" -type "string" "/Users/raquel/Desktop/suelo.psd";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "96C61487-6B44-452C-E638-7782240DBB9F";
createNode aiStandardSurface -n "aiStandardSurface21";
	rename -uid "208702D7-164A-7081-CD5A-79952C8367FD";
createNode shadingEngine -n "aiStandardSurface21SG";
	rename -uid "37D47E51-F748-E8AA-858A-648BB2F597A5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo23";
	rename -uid "C29873F5-6C46-97B7-8E75-8586E289CB23";
createNode groupId -n "groupId93";
	rename -uid "C27E3097-0E4A-B02E-75A6-6BA094B4B5CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId94";
	rename -uid "412AF58B-654A-BFB6-44CD-27871E00D6CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId95";
	rename -uid "F81FE58E-DD4C-F492-3CBB-AE92C9F98776";
	setAttr ".ihi" 0;
createNode groupId -n "groupId96";
	rename -uid "78DA3CFC-D841-FF70-8DFF-F9B69FE635E1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId97";
	rename -uid "5AAC53DC-7B49-FE7E-C646-F892A25F5C3B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId98";
	rename -uid "ECAA2FD7-CD4F-0EAE-E933-4AAC30746C14";
	setAttr ".ihi" 0;
createNode groupId -n "groupId99";
	rename -uid "CDE8E0A2-1540-720F-C34B-73A1989314EB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId100";
	rename -uid "7BFA378A-584D-83C6-3469-E788911ABCC4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId101";
	rename -uid "5229DC81-074E-95BA-D242-3B806C06A9C0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId102";
	rename -uid "76C6F4AF-D14E-0544-9452-A680D9375599";
	setAttr ".ihi" 0;
createNode groupId -n "groupId103";
	rename -uid "194BA893-144D-2763-D887-6C8ECFA8DD0D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId104";
	rename -uid "70C67B33-3A4D-7E2D-B5A6-9E846AFD4C07";
	setAttr ".ihi" 0;
createNode groupId -n "groupId105";
	rename -uid "46A50798-F848-3BCA-1ADF-B7A91B670BF8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId106";
	rename -uid "899D163A-6F40-414D-0F4D-70A28D5C27E0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId107";
	rename -uid "51A08178-E149-EE2A-D841-C09C34A99124";
	setAttr ".ihi" 0;
createNode groupId -n "groupId108";
	rename -uid "8B11B098-124A-0DFF-E6D4-EF9588FC2A9A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId109";
	rename -uid "73D6D0B4-D940-BF4F-FA58-E89F98BB611E";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 90;
	setAttr ".unw" 90;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 25 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 28 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr -s 11 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 10 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId97.id" "House1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "House1Shape.iog.og[0].gco";
connectAttr "groupId98.id" "House1Shape.iog.og[1].gid";
connectAttr "aiStandardSurface9SG.mwc" "House1Shape.iog.og[1].gco";
connectAttr "groupId99.id" "House1Shape.iog.og[2].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[2].gco";
connectAttr "groupId100.id" "House1Shape.iog.og[3].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[3].gco";
connectAttr "groupId101.id" "House1Shape.iog.og[4].gid";
connectAttr "aiStandardSurface18SG.mwc" "House1Shape.iog.og[4].gco";
connectAttr "groupId48.id" "House1Shape.ciog.cog[0].cgid";
connectAttr "groupId104.id" "HouseRoofShape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "HouseRoofShape.iog.og[0].gco";
connectAttr "groupId105.id" "HouseRoofShape.iog.og[1].gid";
connectAttr "aiStandardSurface9SG.mwc" "HouseRoofShape.iog.og[1].gco";
connectAttr "groupId106.id" "HouseRoofShape.iog.og[2].gid";
connectAttr "aiStandardSurface15SG.mwc" "HouseRoofShape.iog.og[2].gco";
connectAttr "groupId51.id" "HouseRoofShape.ciog.cog[0].cgid";
connectAttr "groupId102.id" "WindowShape1.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape1.iog.og[0].gco";
connectAttr "groupId103.id" "WindowShape1.iog.og[1].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape1.iog.og[1].gco";
connectAttr "groupId68.id" "WindowShape1.ciog.cog[0].cgid";
connectAttr "groupId60.id" "WindowShape3.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape3.iog.og[0].gco";
connectAttr "groupId62.id" "WindowShape3.iog.og[1].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape3.iog.og[1].gco";
connectAttr "groupId63.id" "WindowShape3.iog.og[2].gid";
connectAttr "aiStandardSurface5SG.mwc" "WindowShape3.iog.og[2].gco";
connectAttr "groupId61.id" "WindowShape3.ciog.cog[0].cgid";
connectAttr "groupId93.id" "polySurfaceShape36.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "polySurfaceShape36.iog.og[0].gco";
connectAttr "groupId94.id" "polySurfaceShape36.iog.og[2].gid";
connectAttr "aiStandardSurface12SG.mwc" "polySurfaceShape36.iog.og[2].gco";
connectAttr "groupId107.id" "polySurfaceShape37.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "polySurfaceShape37.iog.og[0].gco";
connectAttr "groupId108.id" "WindowShape2.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape2.iog.og[0].gco";
connectAttr "groupId109.id" "WindowShape2.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape2.iog.og[1].gco";
connectAttr "groupId65.id" "WindowShape2.ciog.cog[0].cgid";
connectAttr "groupId84.id" "DoorShape.iog.og[0].gid";
connectAttr "aiStandardSurface5SG.mwc" "DoorShape.iog.og[0].gco";
connectAttr "groupId86.id" "DoorShape.iog.og[1].gid";
connectAttr "aiStandardSurface13SG.mwc" "DoorShape.iog.og[1].gco";
connectAttr "groupId85.id" "DoorShape.ciog.cog[0].cgid";
connectAttr "groupId81.id" "Door1Shape.iog.og[0].gid";
connectAttr "aiStandardSurface5SG.mwc" "Door1Shape.iog.og[0].gco";
connectAttr "groupId83.id" "Door1Shape.iog.og[1].gid";
connectAttr "aiStandardSurface13SG.mwc" "Door1Shape.iog.og[1].gco";
connectAttr "groupId82.id" "Door1Shape.ciog.cog[0].cgid";
connectAttr "groupId70.id" "WindowShape5.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape5.iog.og[0].gco";
connectAttr "groupId72.id" "WindowShape5.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape5.iog.og[1].gco";
connectAttr "groupId71.id" "WindowShape5.ciog.cog[0].cgid";
connectAttr "groupId78.id" "WindowShape6.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape6.iog.og[0].gco";
connectAttr "groupId80.id" "WindowShape6.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape6.iog.og[1].gco";
connectAttr "groupId79.id" "WindowShape6.ciog.cog[0].cgid";
connectAttr "groupId54.id" "WindowShape8.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape8.iog.og[0].gco";
connectAttr "groupId56.id" "WindowShape8.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape8.iog.og[1].gco";
connectAttr "groupId55.id" "WindowShape8.ciog.cog[0].cgid";
connectAttr "groupId57.id" "WindowShape9.iog.og[0].gid";
connectAttr "aiStandardSurface7SG.mwc" "WindowShape9.iog.og[0].gco";
connectAttr "groupId59.id" "WindowShape9.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "WindowShape9.iog.og[1].gco";
connectAttr "groupId58.id" "WindowShape9.ciog.cog[0].cgid";
connectAttr "groupId95.id" "lanternShape.iog.og[0].gid";
connectAttr "aiStandardSurface13SG.mwc" "lanternShape.iog.og[0].gco";
connectAttr "groupId96.id" "lanternShape.iog.og[1].gid";
connectAttr "aiStandardSurface12SG.mwc" "lanternShape.iog.og[1].gco";
connectAttr "groupId88.id" "lanternShape.ciog.cog[0].cgid";
connectAttr "groupId10.id" "millwheel1Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "millwheel1Shape.iog.og[0].gco";
connectAttr "groupId11.id" "millwheel1Shape.ciog.cog[0].cgid";
connectAttr "groupId12.id" "pCylinderShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts3.og" "pCylinderShape1.i";
connectAttr "groupId13.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupId8.id" "pCylinderShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "groupId9.id" "pCylinderShape2.ciog.cog[0].cgid";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardHair1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface7SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface8SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface9SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface10SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiShadowMatte1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface11SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface12SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface13SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface14SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface15SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface16SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface17SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface18SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface19SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface20SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "aiStandardSurface21SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardHair1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface7SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface8SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface9SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface10SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiShadowMatte1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface11SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface12SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface13SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface14SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface15SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface16SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface17SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface18SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface19SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface20SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "aiStandardSurface21SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "polyCylinder4.out" "groupParts3.ig";
connectAttr "groupId12.id" "groupParts3.gi";
connectAttr "verde2.out" "aiStandardSurface1SG.ss";
connectAttr "TreetopSmall1Shape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "TreetopSmall3Shape.iog" "aiStandardSurface1SG.dsm" -na;
connectAttr "aiStandardSurface1SG.msg" "materialInfo1.sg";
connectAttr "verde2.msg" "materialInfo1.m";
connectAttr "verde2.msg" "materialInfo1.t" -na;
connectAttr "verde1.out" "aiStandardSurface2SG.ss";
connectAttr "TreetopBig2Shape.iog" "aiStandardSurface2SG.dsm" -na;
connectAttr "FloorShape.iog" "aiStandardSurface2SG.dsm" -na;
connectAttr "aiStandardSurface2SG.msg" "materialInfo2.sg";
connectAttr "verde1.msg" "materialInfo2.m";
connectAttr "verde1.msg" "materialInfo2.t" -na;
connectAttr "marron.out" "aiStandardSurface3SG.ss";
connectAttr "BigTreeShape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "BranchSmallTree1Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "BranchBigTreeShape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Wood5Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Wood6Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "Step1Shape.iog" "aiStandardSurface3SG.dsm" -na;
connectAttr "aiStandardSurface3SG.msg" "materialInfo3.sg";
connectAttr "marron.msg" "materialInfo3.m";
connectAttr "marron.msg" "materialInfo3.t" -na;
connectAttr "aiStandardHair1.out" "aiStandardHair1SG.ss";
connectAttr "aiStandardHair1SG.msg" "materialInfo4.sg";
connectAttr "aiStandardHair1.msg" "materialInfo4.m";
connectAttr "aiStandardHair1.msg" "materialInfo4.t" -na;
connectAttr "aiStandardSurface4.out" "aiStandardSurface4SG.ss";
connectAttr "aiStandardSurface4SG.msg" "materialInfo5.sg";
connectAttr "aiStandardSurface4.msg" "materialInfo5.m";
connectAttr "aiStandardSurface4.msg" "materialInfo5.t" -na;
connectAttr "marron_claro.out" "aiStandardSurface5SG.ss";
connectAttr "SmallTreeShape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "BranchSmallTreeShape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "polySurfaceShape10.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "SmallTree1Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "polySurfaceShape15.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "Column2Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "Column1Shape.iog" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape3.iog.og[2]" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape3.iog.og[1]" "aiStandardSurface5SG.dsm" -na;
connectAttr "Door1Shape.iog.og[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "Door1Shape.ciog.cog[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "DoorShape.iog.og[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "DoorShape.ciog.cog[0]" "aiStandardSurface5SG.dsm" -na;
connectAttr "WindowShape1.iog.og[1]" "aiStandardSurface5SG.dsm" -na;
connectAttr "groupId63.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId62.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId81.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId82.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId84.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId85.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "groupId103.msg" "aiStandardSurface5SG.gn" -na;
connectAttr "aiStandardSurface5SG.msg" "materialInfo6.sg";
connectAttr "marron_claro.msg" "materialInfo6.m";
connectAttr "marron_claro.msg" "materialInfo6.t" -na;
connectAttr "verde_claro.out" "aiStandardSurface6SG.ss";
connectAttr "TreetopSmall2Shape.iog" "aiStandardSurface6SG.dsm" -na;
connectAttr "TreetopBig1Shape.iog" "aiStandardSurface6SG.dsm" -na;
connectAttr "aiStandardSurface6SG.msg" "materialInfo7.sg";
connectAttr "verde_claro.msg" "materialInfo7.m";
connectAttr "verde_claro.msg" "materialInfo7.t" -na;
connectAttr "marronClaro.out" "aiStandardSurface7SG.ss";
connectAttr "millWheelShape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladder5Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladderShape7.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladderShape4.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "ladder6Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column5Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column4Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column3Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "Column6Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape8.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape8.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape9.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape9.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape3.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape3.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape2.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape1.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape5.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape5.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape6.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape6.ciog.cog[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "tableShape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table4Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table3Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table2Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "table1Shape.iog" "aiStandardSurface7SG.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape1.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "polySurfaceShape37.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "WindowShape2.iog.og[0]" "aiStandardSurface7SG.dsm" -na;
connectAttr "groupId54.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId55.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId57.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId58.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId60.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId61.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId65.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId68.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId70.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId71.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId78.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId79.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId93.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId102.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId107.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "groupId108.msg" "aiStandardSurface7SG.gn" -na;
connectAttr "aiStandardSurface7SG.msg" "materialInfo8.sg";
connectAttr "marronClaro.msg" "materialInfo8.m";
connectAttr "marronClaro.msg" "materialInfo8.t" -na;
connectAttr "MarronMuyClaro.out" "aiStandardSurface8SG.ss";
connectAttr "polySurfaceShape29.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "pawsShape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "paws2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "MillWheel2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "FloorShape2.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "polySurfaceShape31.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof1Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof2Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof3Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "Roof4Shape.iog" "aiStandardSurface8SG.dsm" -na;
connectAttr "aiStandardSurface8SG.msg" "materialInfo9.sg";
connectAttr "MarronMuyClaro.msg" "materialInfo9.m";
connectAttr "MarronMuyClaro.msg" "materialInfo9.t" -na;
connectAttr "Rojo.out" "aiStandardSurface9SG.ss";
connectAttr "polySurfaceShape30.iog" "aiStandardSurface9SG.dsm" -na;
connectAttr "House1Shape.iog.og[1]" "aiStandardSurface9SG.dsm" -na;
connectAttr "HouseRoofShape.iog.og[1]" "aiStandardSurface9SG.dsm" -na;
connectAttr "groupId98.msg" "aiStandardSurface9SG.gn" -na;
connectAttr "groupId105.msg" "aiStandardSurface9SG.gn" -na;
connectAttr "aiStandardSurface9SG.msg" "materialInfo10.sg";
connectAttr "Rojo.msg" "materialInfo10.m";
connectAttr "Rojo.msg" "materialInfo10.t" -na;
connectAttr "azulClaro.out" "aiStandardSurface10SG.ss";
connectAttr "polySurfaceShape16.iog" "aiStandardSurface10SG.dsm" -na;
connectAttr "aiStandardSurface10SG.msg" "materialInfo11.sg";
connectAttr "azulClaro.msg" "materialInfo11.m";
connectAttr "azulClaro.msg" "materialInfo11.t" -na;
connectAttr "aiShadowMatte1.out" "aiShadowMatte1SG.ss";
connectAttr "aiShadowMatte1SG.msg" "materialInfo12.sg";
connectAttr "aiShadowMatte1.msg" "materialInfo12.m";
connectAttr "aiShadowMatte1.msg" "materialInfo12.t" -na;
connectAttr "aiStandardSurface11.out" "aiStandardSurface11SG.ss";
connectAttr "aiStandardSurface11SG.msg" "materialInfo13.sg";
connectAttr "aiStandardSurface11.msg" "materialInfo13.m";
connectAttr "aiStandardSurface11.msg" "materialInfo13.t" -na;
connectAttr "amarillo.out" "aiStandardSurface12SG.ss";
connectAttr "WindowShape8.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape9.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape5.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape6.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "polySurfaceShape36.iog.og[2]" "aiStandardSurface12SG.dsm" -na;
connectAttr "lanternShape.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "WindowShape2.iog.og[1]" "aiStandardSurface12SG.dsm" -na;
connectAttr "groupId56.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId59.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId72.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId80.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId94.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId96.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "groupId109.msg" "aiStandardSurface12SG.gn" -na;
connectAttr "aiStandardSurface12SG.msg" "materialInfo14.sg";
connectAttr "amarillo.msg" "materialInfo14.m";
connectAttr "amarillo.msg" "materialInfo14.t" -na;
connectAttr "negro.out" "aiStandardSurface13SG.ss";
connectAttr "groupId83.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId86.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId88.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "groupId95.msg" "aiStandardSurface13SG.gn" -na;
connectAttr "Door1Shape.iog.og[1]" "aiStandardSurface13SG.dsm" -na;
connectAttr "DoorShape.iog.og[1]" "aiStandardSurface13SG.dsm" -na;
connectAttr "lanternShape.ciog.cog[0]" "aiStandardSurface13SG.dsm" -na;
connectAttr "lanternShape.iog.og[0]" "aiStandardSurface13SG.dsm" -na;
connectAttr "aiStandardSurface13SG.msg" "materialInfo15.sg";
connectAttr "negro.msg" "materialInfo15.m";
connectAttr "negro.msg" "materialInfo15.t" -na;
connectAttr "amarilloClaro.out" "aiStandardSurface14SG.ss";
connectAttr "CoverShape1.iog" "aiStandardSurface14SG.dsm" -na;
connectAttr "CoverShape2.iog" "aiStandardSurface14SG.dsm" -na;
connectAttr "aiStandardSurface14SG.msg" "materialInfo16.sg";
connectAttr "amarilloClaro.msg" "materialInfo16.m";
connectAttr "amarilloClaro.msg" "materialInfo16.t" -na;
connectAttr "morado.out" "aiStandardSurface15SG.ss";
connectAttr "groupId106.msg" "aiStandardSurface15SG.gn" -na;
connectAttr "HouseRoofShape.iog.og[2]" "aiStandardSurface15SG.dsm" -na;
connectAttr "aiStandardSurface15SG.msg" "materialInfo17.sg";
connectAttr "morado.msg" "materialInfo17.m";
connectAttr "morado.msg" "materialInfo17.t" -na;
connectAttr "aiStandardSurface16.out" "aiStandardSurface16SG.ss";
connectAttr "aiStandardSurface16SG.msg" "materialInfo18.sg";
connectAttr "aiStandardSurface16.msg" "materialInfo18.m";
connectAttr "aiStandardSurface16.msg" "materialInfo18.t" -na;
connectAttr "aiStandardSurface17.out" "aiStandardSurface17SG.ss";
connectAttr "aiStandardSurface17SG.msg" "materialInfo19.sg";
connectAttr "aiStandardSurface17.msg" "materialInfo19.m";
connectAttr "aiStandardSurface17.msg" "materialInfo19.t" -na;
connectAttr "moradoClaro.out" "aiStandardSurface18SG.ss";
connectAttr "House1Shape.iog.og[2]" "aiStandardSurface18SG.dsm" -na;
connectAttr "House1Shape.iog.og[3]" "aiStandardSurface18SG.dsm" -na;
connectAttr "House1Shape.iog.og[4]" "aiStandardSurface18SG.dsm" -na;
connectAttr "groupId99.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "groupId100.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "groupId101.msg" "aiStandardSurface18SG.gn" -na;
connectAttr "aiStandardSurface18SG.msg" "materialInfo20.sg";
connectAttr "moradoClaro.msg" "materialInfo20.m";
connectAttr "moradoClaro.msg" "materialInfo20.t" -na;
connectAttr "gris.out" "aiStandardSurface19SG.ss";
connectAttr "StepShape.iog" "aiStandardSurface19SG.dsm" -na;
connectAttr "Step2Shape.iog" "aiStandardSurface19SG.dsm" -na;
connectAttr "aiStandardSurface19SG.msg" "materialInfo21.sg";
connectAttr "gris.msg" "materialInfo21.m";
connectAttr "gris.msg" "materialInfo21.t" -na;
connectAttr "file1.oc" "aiStandardSurface20.base_color";
connectAttr "aiStandardSurface20.out" "aiStandardSurface20SG.ss";
connectAttr "aiStandardSurface20SG.msg" "materialInfo22.sg";
connectAttr "aiStandardSurface20.msg" "materialInfo22.m";
connectAttr "file1.msg" "materialInfo22.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "aiStandardSurface21.out" "aiStandardSurface21SG.ss";
connectAttr "aiStandardSurface21SG.msg" "materialInfo23.sg";
connectAttr "aiStandardSurface21.msg" "materialInfo23.m";
connectAttr "aiStandardSurface21.msg" "materialInfo23.t" -na;
connectAttr "aiStandardSurface1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface2SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface3SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardHair1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface4SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface5SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface6SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface7SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface8SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface9SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface10SG.pa" ":renderPartition.st" -na;
connectAttr "aiShadowMatte1SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface11SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface12SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface13SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface14SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface15SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface16SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface17SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface18SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface19SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface20SG.pa" ":renderPartition.st" -na;
connectAttr "aiStandardSurface21SG.pa" ":renderPartition.st" -na;
connectAttr "verde2.msg" ":defaultShaderList1.s" -na;
connectAttr "verde1.msg" ":defaultShaderList1.s" -na;
connectAttr "marron.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardHair1.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface4.msg" ":defaultShaderList1.s" -na;
connectAttr "marron_claro.msg" ":defaultShaderList1.s" -na;
connectAttr "verde_claro.msg" ":defaultShaderList1.s" -na;
connectAttr "marronClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "MarronMuyClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "Rojo.msg" ":defaultShaderList1.s" -na;
connectAttr "azulClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "aiShadowMatte1.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface11.msg" ":defaultShaderList1.s" -na;
connectAttr "amarillo.msg" ":defaultShaderList1.s" -na;
connectAttr "negro.msg" ":defaultShaderList1.s" -na;
connectAttr "amarilloClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "morado.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface16.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface17.msg" ":defaultShaderList1.s" -na;
connectAttr "moradoClaro.msg" ":defaultShaderList1.s" -na;
connectAttr "gris.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface20.msg" ":defaultShaderList1.s" -na;
connectAttr "aiStandardSurface21.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "HumanShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "millwheel1Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "House1Shape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "HouseRoofShape.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "House1Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "HouseRoofShape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId11.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId13.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId48.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId51.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId97.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId104.msg" ":initialShadingGroup.gn" -na;
// End of actividad.ma
